#!/usr/bin/ksh
###########################################
## Note:         To take Config Backup to NIM #
#              Server
#
###########################################

DATE=`date +%d%m%Y`
TIME1=`date +%d/%m/%Y_%H:%M:%S`
LOG="/export/nim/log/nim_config.$DATE"
CONFIGDIR="/export/nim/config/fihe7/"

echo "NIM Config Backup started .........`date +%d/%m/%Y_%H:%M:%S` " >> $LOG

for mach in $(lsnim -c machines | grep -Ev "infa|^master"| awk '{print $1}')
        do
        echo "=========================================================================" >> $LOG
        TIME1=`date +%d/%m/%Y_%H:%M:%S`
        echo "STATUS: NIM config backup for $mach initiated on "$TIME1 >> $LOG

        # generates a new config data
        echo "\nIssuing Command:\/usr/lpp/bos.sysmgt/nim/methods/m_cust -a script=aix_info $mach >>$CONFIGDIR${mach}_config_$DATE 2>&1" >> $LOG 2>&1
        /usr/lpp/bos.sysmgt/nim/methods/m_cust -a script=aix_info $mach > $CONFIGDIR${mach}_config_$DATE 2>&1
        if [ $? != 0 ]
                then
                echo "STATUS: config backup error on ${mach}" >> $LOG
        else
                TIME1=`date +%d/%m/%Y_%H:%M:%S`
                echo "STATUS: config backup for ${mach} completed successfully." $TIME1 >> $LOG
        fi
        echo "STATUS: NIM config backup for $mach completed on "$DATE >> $LOG

        # removes old config files keeping 1 copies in the config directory
        echo "STATUS: Remove number old config backup on: "$TIME1 >> $LOG
        FILECOUNT=`ls -Al $CONFIGDIR | grep $mach | wc -l`
        if [[ $FILECOUNT -gt  1 ]]; then
                RMCFFILE=`ls -lt $CONFIGDIR | grep $mach | tail -1 | awk '{print $9}'`
                echo "STATUS: Removing: " $RMCFFILE  1 >> $LOG
                echo "rm $CONFIGDIR$RMCFFILE" >> $LOG
                rm $CONFIGDIR$RMCFFILE 1>>$LOG 2>&1
        else
                echo "STATUS: Number of config files: " $FILECOUNT >> $LOG
                echo "STATUS: No old config file to clear for $mach" >> $LOG
        fi

        echo "=========================================================================\n" >> $LOG
done

if [ $? -eq 0 ]
then
        echo "STATUS: NIM config backup ended SUCCESSFULLY .........`date +%Y/%m/%d_%H:%M:%S` " >> $LOG
else
        echo "STATUS: NIM config backup Ended with FAILURE ............ `date +%Y/%m/%d_%H:%M:%S` " >> $LOG

fi

# CONTENT Could be piped into an email so that a status email could be sent after every instance of script ran
MACHCOUNT=`lsnim -c machines | grep -Ev "infa|^master" | wc -l`
MKCOUNT=`ls $CONFIGDIR | grep $DATE | wc -l`
CONTENT="/export/nim/config/config_content.txt"
ERRORS=`cat $LOG | egrep -i "warn|fail|err" | egrep -v "Standard|0511-449|0511-084" | wc -l`
MACHINES=`lsnim -c machines | grep -Ev "infa|^master" | awk '{print $1}'`

echo "=========================================================================\n" > $CONTENT
TIME1=`date +%Y/%m/%d_%H:%M:%S`
echo "NIM Server config backup summary generated on "$TIME1 >> $CONTENT
echo "\nTotal of $MKCOUNT out of $MACHCOUNT servers backed up\n" >> $CONTENT
if [[ $MKCOUNT !=  $MACHCOUNT ]]; then
        echo "\n** Config Backup Status:  The number of config backups performed does not match total number of machines : FAILED" >> $CONTENT
else
        echo "\nConfig Backup Status: All defined machines configuration backed up: OK" >> $CONTENT
fi

if [[ $ERRORS -gt 0 ]]; then
        ERRSTS="FAILED"
else
        ERRSTS="GOOD"
fi

echo "\nERROR STATUS: There are $ERRORS issue in log file : $ERRSTS" >> $CONTENT

echo "\nFollowing Servers did not back up properly: "  >> $CONTENT
for i in $MACHINES; do
	cat $CONFIGDIR$i"_config_"$DATE|grep -E "0042-006|0042-069"  1>>$LOG 2>&1
        if [[ $? = 0 ]]; then
                echo $i  >> $CONTENT
        fi
done

# You may put a line below to mail the CONTENT to the sysadmins
cat $CONTENT | mailx -s "CONFIG BACKUP STATUS of HE7 AIX SERVERS" -r fihe7.nim@tryg.dk tryg.unix.dist@tryg.dk

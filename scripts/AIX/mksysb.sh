#!/usr/bin/ksh
###########################################
## Note:         To back up mksysb to NIM #
#              Server
# 
###########################################

DAY=`date +%d`
if [[ "$DAY" -gt 21 && "$DAY" -lt 29 ]]
then
DATE=`date +%Y%m%d`
TIME1=`date +%Y/%m/%d_%H:%M:%S`
LOG="/export/nim/log/nim_mksysb.$DATE"
MKSYSBDIR="/export/nim/mksysb/"

echo "NIM Backup started .........`date +%Y/%m/%d_%H:%M:%S` " >> $LOG

for mach in $(lsnim -c machines | grep -v ^master |grep -v dkdc1cls |grep -v dkdc1tst | awk '{print $1}')
        do
        echo "=========================================================================" >> $LOG
        TIME1=`date +%Y/%m/%d_%H:%M:%S`
        echo "STATUS: NIM mksysb for $mach initiated on "$TIME1 >> $LOG

        # generates a new mksysb
        echo "\nIssuing Command:\nnim -o define -t mksysb -a server=master -a location=$MKSYSBDIR${mach}_mksysb_$DATE -a source=$mach -a mk_image=yes -a mksysb_flags="-i -m -e" -F ${mach}_mksysb_$DATE" >> $LOG 2>&1
        nim -o define -t mksysb -a server=master -a location=$MKSYSBDIR${mach}_mksysb_$DATE -a source=$mach -a mk_image=yes -a mksysb_flags="-i -m -e" -F ${mach}_mksysb_$DATE 1>>$LOG 2>&1
        if [ $? != 0 ]
                then
                echo "STATUS: mksysb error on ${mach}" >> $LOG
        else
                TIME1=`date +%Y/%m/%d_%H:%M:%S`
                echo "STATUS: mksysb for ${mach} completed successfully." $TIME1 >> $LOG
        fi
        echo "STATUS: NIM mksysb for $mach completed on "$DATE >> $LOG

        # removes old mksysb keeping 1 copies in the mksysb directory
        echo "STATUS: Remove number old mksysb on: "$TIME1 >> $LOG
        FILECOUNT=`ls -Al $MKSYSBDIR | grep $mach | wc -l`
        if [[ $FILECOUNT -gt  1 ]]; then
                RMBKFILE=`ls -lt $MKSYSBDIR | grep $mach | tail -1 | awk '{print $9}'`
                echo "STATUS: Removing: " $RMBKFILE 1 >> $LOG
                echo "nim -o remove $RMBKFILE, number of files: $FILECOUNT" >> $LOG
                nim -o remove -a rm_image=yes $RMBKFILE 1>>$LOG 2>&1
        else
                echo "STATUS: Number of mksysb files: " $FILECOUNT >> $LOG
                echo "STATUS: No old mksysb's to clear for $mach" >> $LOG
        fi

        echo "=========================================================================\n" >> $LOG
done

if [ $? -eq 0 ]
then
        echo "STATUS: NIM mksysb backup ended SUCCESSFULLY .........`date +%Y/%m/%d_%H:%M:%S` " >> $LOG
else
        echo "STATUS: NIM mksysb backup Ended with FAILURE ............ `date +%Y/%m/%d_%H:%M:%S` " >> $LOG

fi

# CONTENT Could be piped into an email so that a status email could be sent after every instance of script ran
MACHCOUNT=`lsnim -c machines | grep -v ^master |grep -v dkdc1cls |grep -v dkdc1tst | wc -l`
MKCOUNT=`ls $MKSYSBDIR | grep $DATE | wc -l`
CONTENT="/export/nim/mksysb/content.txt"
ERRORS=`cat $LOG | egrep -i "warn|fail|err" | egrep -v "Standard|0511-449|0511-084" | wc -l`
MACHINES=`lsnim -c machines | grep -v ^master | grep -v dkdc1cls |grep -v dkdc1tst | awk '{print $1}'`

echo "=========================================================================\n" > $CONTENT
TIME1=`date +%Y/%m/%d_%H:%M:%S`
echo "NIM Server backup summary generated on "$TIME1 >> $CONTENT
echo "\nTotal of $MKCOUNT out of $MACHCOUNT servers backed up\n" >> $CONTENT
if [[ $MKCOUNT !=  $MACHCOUNT ]]; then
        echo "\n** MKSYSB Status:  The number of mksysb's performed does not match total number of machines : FAILED" >> $CONTENT
	echo "\nPlease check $LOG for troubleshooting." >> $CONTENT
else
        echo "\nMKSYSB Status: All defined machines backed up: OK" >> $CONTENT
fi

if [[ $ERRORS -gt 0 ]]; then
        ERRSTS="FAILED"
else
        ERRSTS="GOOD"
fi

echo "\nERROR STATUS: There are $ERRORS issue(s) in log file : $ERRSTS" >> $CONTENT

echo "\nFollowing Servers did not back up properly: "  >> $CONTENT
for i in $MACHINES; do
        ls $MKSYSBDIR$i"_mksysb_"$DATE  1>>$LOG 2>&1
        if [[ $? != 0 ]]; then
                echo $i  >> $CONTENT
        fi
done

# You may put a line below to mail the CONTENT to the sysadmins
cat $CONTENT | mailx -s "MKSYSB STATUS of HE7 AIX SERVERS" -r fihe7.nim@tryg.dk tryg.unix.dist@tryg.dk
fi

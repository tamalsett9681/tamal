#!/usr/bin/ksh

CLONEVG=`lsvg|grep "_rootvg"`
if [ "$CLONEVG" ]
then
CLONEDISK=`lspv|grep $CLONEVG|awk '{print $1}'`
/usr/sbin/exportvg $CLONEVG
/usr/sbin/alt_disk_copy -P all -B -d $CLONEDISK
else
for disk in hdisk0 hdisk1
do
/usr/sbin/lspv $disk > /dev/null 2>&1
if [ "$?" == 1 ]
then /usr/sbin/alt_disk_copy -P all -B -d $disk
fi
done
fi

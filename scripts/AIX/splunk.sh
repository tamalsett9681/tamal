/opt/splunkforwarder/bin/splunk stop
rm -rf /opt/splunkforwarder
/usr/bin/tar -xvf /var/tmp/splunkforwarder-7.3.3-7af3758d0d5e-AIX-powerpc.tar -C /opt/
/opt/splunkforwarder/bin/splunk start --accept-license --gen-and-print-passwd 
/opt/splunkforwarder/bin/splunk enable boot-start
/usr/bin/cp -f /var/tmp/deploymentclient.conf /opt/splunkforwarder/etc/system/default/
/usr/bin/stopsrc -s splunkd
/usr/bin/kill -9 `ps -ef|grep splunkd|grep -v process-runner|grep -v grep|awk '{print $2}'`
/usr/bin/startsrc -s splunkd

#!/usr/bin/ksh
# This Script is intended to print disk drive details of all AIX Servers not VG-wise
# This Script can be run from any user

n=1
echo "*************************************************************"
echo "*************************************************************"
echo "Available hdisks are :"
echo "----------------------"
echo "Hdisk Name\t\tCapacity (in GB)\t\tLun ID\t\t\t\t\tVG Name"
echo "----------------------------------------------------------------------------------------------------------------"
#for i in `lspv|awk '{print $1}'`
lspv|while read hdisk pvid vg_name actv
do cap=`expr $(getconf DISK_SIZE /dev/$hdisk) / 1024`
lun_id=$(lscfg -vpl $hdisk|grep Serial|tail -c 25)
serial=`lscfg -vpl $hdisk|grep Serial|xargs|cut -d' ' -f3`
echo "$n.$hdisk\t\t\t$cap\t\t\t$lun_id\t\t$vg_name"
n=`expr $n + 1`
done
echo "*************************************************************"
echo "*************************************************************"

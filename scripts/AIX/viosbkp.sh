#!/bin/sh
# This Script will automatically take the mksysb backup of VIOS without Media Library

mount /viosbkp
VALUE=`echo $?`
if [ $VALUE == 0 ]
then
/usr/ios/cli/ioscli backupios -file /viosbkp/fihe7vioa001-mksysb-`date +%d%m%Y` -mksysb -nomedialib
umount -f /viosbkp
fi

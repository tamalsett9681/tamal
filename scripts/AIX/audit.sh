/usr/sbin/mklv -y auditlv -t jfs2 -x 512 rootvg 16
/usr/sbin/crfs -v jfs2 -d auditlv -m /audit -A yes -p rw -a agblksize=4096 -a logname='hd8' -a isnapshot='no'
/usr/sbin/mount /audit
/usr/sbin/chfs -a size=2G /audit
/usr/bin/cp /var/tmp/audit-log-copy.sh /audit/
/usr/bin/cp /var/tmp/audit_st*.sh /etc/rc.d/init.d
/usr/bin/ln /etc/rc.d/init.d/audit_start.sh /etc/rc.d/rc2.d/S80Audit
/usr/bin/ln /etc/rc.d/init.d/audit_stop.sh /etc/rc.d/rc2.d/K80Audit
/usr/bin/echo "0 0 * * * /audit/audit-log-copy.sh >/dev/null 2>&1      #Everyday audit log copy" >> /var/spool/cron/crontabs/root
/usr/bin/echo '0 3 * * * /usr/bin/find /audit -name "*.log.gz" -mtime +90 -exec rm {} \\\;         # Delete older Audit Logs' >> /var/spool/cron/crontabs/root
/usr/sbin/audit shutdown
/usr/bin/cp /var/tmp/config /etc/security/audit/config
/usr/sbin/audit start 1>&- 2>&-

if id oracle > /dev/null 2>&1
then
chuser unsuccessful_login_count=0 oracle
echo "oracle:TrygOraDba#54321"|chpasswd -c
lastupdate=`pwdadm -q oracle|grep lastupdate|xargs|cut -d' ' -f3`
last=`perl -we 'print(my $time = localtime '$lastupdate', "\n")'`
echo "Last date of password change for oracle: $last"
fi

if id tcsdba > /dev/null 2>&1
then
chuser unsuccessful_login_count=0 tcsdba
echo "tcsdba:TrygOraDba#54321"|chpasswd -c
lastupdate=`pwdadm -q tcsdba|grep lastupdate|xargs|cut -d' ' -f3`
last=`perl -we 'print(my $time = localtime '$lastupdate', "\n")'`
echo "Last date of password change for tcsdba: $last"
fi

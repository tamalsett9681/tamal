#!/bin/ksh
### This is the first script of the complete workflow for TIARAP cloning from TIAPROD which in turn calls the oratab checking script /root/tia-scripts/2-oratab-check.sh ####
### Prepared by: Subhayan Das
### Date: 25-Sept-2019
### Date: 22-Sep-2022 - changed /home/tia/SnapMgr/BACKUP_START to /home/tia/SnapMgr/CLONE_START_TIARAP
#######################################################################################################################################################################
#if [ -f /home/tia/SnapMgr/CLONE_START_TIARAP || -f /home/tia/SnapMgr/BACKUP_START ]
if [ -f /home/tia/SnapMgr/CLONE_START_TIARAP -o -f /home/tia/SnapMgr/BACKUP_START ]
then
 if [ -f /home/tia/SnapMgr/NO_TIARAP ]
 then
 rm /home/tia/SnapMgr/CLONE_START_TIARAP
 rm /home/tia/SnapMgr/BACKUP_START
 echo "NO_TIARAP file found at `date '+%d'-'%m'-'%y','%H':'%M'`. Hence cannot proceed with Snap Cloning for TIARAP."|mailx -s "Snap Cloning for TIARAP Status" -r alkapodb005.alkait.net@tryg.dk tryg.unix.dist@tryg.dk tcs.database@tryg.dk satadru.das@tryg.dk
else
#echo "`date '+%d'-'%m'-'%Y'_'%H'-'%M'`" > /root/tia-scripts/date.txt
#rm /home/tia/SnapMgr/BACKUP_START  &&  sh /root/tia-scripts/final.sh
rm /home/tia/SnapMgr/BACKUP_START
rm /home/tia/SnapMgr/CLONE_START_TIARAP  
sh /root/tia-scripts/New_Tia_refresh.sh
 fi
fi

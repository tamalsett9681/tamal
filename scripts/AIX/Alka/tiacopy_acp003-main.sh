if [ -f /home/tia/SnapMgr/CLONE_START_TIACOPY ]
then
 if [ -f /home/tia/SnapMgr/NO_TIACOPY ]
 then
  rm /home/tia/SnapMgr/CLONE_START_TIACOPY
  echo "NO_TIACOPY file found at `date '+%d'-'%m'-'%y','%H':'%M'`. Hence cannot proceed with Snap Cloning for TIACOPY and ACP003."|mailx -s "Snap Cloning for TIARAP Status" -r alkapodb005.alkait.net@tryg.dk tryg.unix.dist@tryg.dk tcs.database@tryg.dk satadru.das@tryg.dk
 else
  rm /home/tia/SnapMgr/CLONE_START_TIACOPY && su - oracle -c "/opt/oracle/TCS/script/ACP003_Refresh/Acp003_ActiveDuplicate.sh &"
  su - oracle -c "/opt/oracle/TCS/script/TIACOPY_Refresh/Tiacopy_ActiveDuplicate.sh"
 fi
fi

#! /bin/ksh

User="puresnap"              # Pure user on the FlashArray

Array="172.21.17.42"        # Pure FlashArray, hostname or IP address

SrcPG="TRYG-TIA-SNAP-PG"  # Source Protection Group that contains data & redo

SUFFIX=`date +%s`

LOGFILE=/root/tia-scripts/logs/TIARAP_Clone_`date '+%d'-'%m'-'%Y'_'%H'-'%M'`.log

###############################SNAPSHOT_CREATION######################################

puresnap() {

# The snapshot will include the suffix of Unix epoch time

su - ecptia -c "ssh ${User}@${Array} purepgroup snap --suffix DM1-$SUFFIX ${SrcPG} --apply-retention"

}

###############################UNMOUNTING_FILESYSTEM######################################

unmountfs()
 {
ct=$(ps -ef|grep -w ora_smon_TIARAP|grep -v grep)
if [ "${#ct}" -eq 0 ]; then
 #Replace the following with relevant mount details for your environment
 
umount -f /data/ora_data21
umount -f /data/ora_temp21
umount -f /data/ora_arch21
umount -f /data/ora_redo21
umount -f /data/ora_redo22
else
  echo "Cannot unmount, check the usage on the mount !" >> $LOGFILE
  exit -1
fi
}

###############################MOUNTING SNAPSHOT######################################

mountsnap() {

echo "Copying source volume 1 snapshot onto cloned Target volume 1"

su - ecptia -c "ssh ${User}@${Array} purevol copy --overwrite ${SrcPG}.DM1-$SUFFIX.OS-TRYG-TIA-SNAP-SRC-VG/TRYG-DKDC1ALKAPODB003-T0-V1 OS-TRYG-TIA-SNAP-TRGT-VG/TRYG-DKDC1ALKAPODB005-T0-TRGT-V1"

echo "Copying source volume 2 snapshot onto cloned Target volume 2"

su - ecptia -c "ssh ${User}@${Array} purevol copy --overwrite ${SrcPG}.DM1-$SUFFIX.OS-TRYG-TIA-SNAP-SRC-VG/TRYG-DKDC1ALKAPODB003-T0-V2 OS-TRYG-TIA-SNAP-TRGT-VG/TRYG-DKDC1ALKAPODB005-T0-TRGT-V2"

echo "Copying source volume 3 snapshot onto cloned Target volume 3"

su - ecptia -c "ssh ${User}@${Array} purevol copy --overwrite ${SrcPG}.DM1-$SUFFIX.OS-TRYG-TIA-SNAP-SRC-VG/TRYG-DKDC1ALKAPODB003-T0-V3 OS-TRYG-TIA-SNAP-TRGT-VG/TRYG-DKDC1ALKAPODB005-T0-TRGT-V3"

echo "Copying source volume 4 snapshot onto cloned Target volume 4"

su - ecptia -c "ssh ${User}@${Array} purevol copy --overwrite ${SrcPG}.DM1-$SUFFIX.OS-TRYG-TIA-SNAP-SRC-VG/TRYG-DKDC1ALKAPODB003-T0-V4 OS-TRYG-TIA-SNAP-TRGT-VG/TRYG-DKDC1ALKAPODB005-T0-TRGT-V4"

echo "Copying source volume 5 snapshot onto cloned Target volume 5"

su - ecptia -c "ssh ${User}@${Array} purevol copy --overwrite ${SrcPG}.DM1-$SUFFIX.OS-TRYG-TIA-SNAP-SRC-VG/TRYG-DKDC1ALKAPODB003-T0-V5 OS-TRYG-TIA-SNAP-TRGT-VG/TRYG-DKDC1ALKAPODB005-T0-TRGT-V5"

echo "Copying source volume 6 snapshot onto cloned Target volume 6"

su - ecptia -c "ssh ${User}@${Array} purevol copy --overwrite ${SrcPG}.DM1-$SUFFIX.OS-TRYG-TIA-SNAP-SRC-VG/TRYG-DKDC1ALKAPODB003-T0-V6 OS-TRYG-TIA-SNAP-TRGT-VG/TRYG-DKDC1ALKAPODB005-T0-TRGT-V6"

echo "Copying source volume 7 snapshot onto cloned Target volume 7"

su - ecptia -c "ssh ${User}@${Array} purevol copy --overwrite ${SrcPG}.DM1-$SUFFIX.OS-TRYG-TIA-SNAP-SRC-VG/TRYG-DKDC1ALKAPODB003-T0-V7 OS-TRYG-TIA-SNAP-TRGT-VG/TRYG-DKDC1ALKAPODB005-T0-TRGT-V7"

echo "Copying source volume 8 snapshot onto cloned Target volume 8"

su - ecptia -c "ssh ${User}@${Array} purevol copy --overwrite ${SrcPG}.DM1-$SUFFIX.OS-TRYG-TIA-SNAP-SRC-VG/TRYG-DKDC1ALKAPODB003-T0-V8 OS-TRYG-TIA-SNAP-TRGT-VG/TRYG-DKDC1ALKAPODB005-T0-TRGT-V8"

echo "Copying source volume 9 snapshot onto cloned Target volume 9"

su - ecptia -c "ssh ${User}@${Array} purevol copy --overwrite ${SrcPG}.DM1-$SUFFIX.OS-TRYG-TIA-SNAP-SRC-VG/TRYG-DKDC1ALKAPODB003-T0-V9 OS-TRYG-TIA-SNAP-TRGT-VG/TRYG-DKDC1ALKAPODB005-T0-TRGT-V9"

echo "Copying source volume 10 snapshot onto cloned Target volume 10"

su - ecptia -c "ssh ${User}@${Array} purevol copy --overwrite ${SrcPG}.DM1-$SUFFIX.OS-TRYG-TIA-SNAP-SRC-VG/TRYG-DKDC1ALKAPODB003-T0-V10 OS-TRYG-TIA-SNAP-TRGT-VG/TRYG-DKDC1ALKAPODB005-T0-TRGT-V10"

echo "Copying source volume 11 snapshot onto cloned Target volume 11"

su - ecptia -c "ssh ${User}@${Array} purevol copy --overwrite ${SrcPG}.DM1-$SUFFIX.OS-TRYG-TIA-SNAP-SRC-VG/TRYG-DKDC1ALKAPODB003-T0-V11 OS-TRYG-TIA-SNAP-TRGT-VG/TRYG-DKDC1ALKAPODB005-T0-TRGT-V11"

echo "Copying source volume 12 snapshot onto cloned Target volume 12"

su - ecptia -c "ssh ${User}@${Array} purevol copy --overwrite ${SrcPG}.DM1-$SUFFIX.OS-TRYG-TIA-SNAP-SRC-VG/TRYG-DKDC1ALKAPODB003-T0-V12 OS-TRYG-TIA-SNAP-TRGT-VG/TRYG-DKDC1ALKAPODB005-T0-TRGT-V12"

echo "Copying source volume 13 snapshot onto cloned Target volume 13"

su - ecptia -c "ssh ${User}@${Array} purevol copy --overwrite ${SrcPG}.DM1-$SUFFIX.OS-TRYG-TIA-SNAP-SRC-VG/TRYG-DKDC1ALKAPODB003-T0-V13 OS-TRYG-TIA-SNAP-TRGT-VG/TRYG-DKDC1ALKAPODB005-T0-TRGT-V13"

echo "Copying source volume 14 snapshot onto cloned Target volume 14"

su - ecptia -c "ssh ${User}@${Array} purevol copy --overwrite ${SrcPG}.DM1-$SUFFIX.OS-TRYG-TIA-SNAP-SRC-VG/TRYG-DKDC1ALKAPODB003-T0-V14 OS-TRYG-TIA-SNAP-TRGT-VG/TRYG-DKDC1ALKAPODB005-T0-TRGT-V14"

echo "Copying source volume 15 snapshot onto cloned Target volume 15"

su - ecptia -c "ssh ${User}@${Array} purevol copy --overwrite ${SrcPG}.DM1-$SUFFIX.OS-TRYG-TIA-SNAP-SRC-VG/TRYG-DKDC1ALKAPODB003-T0-V15 OS-TRYG-TIA-SNAP-TRGT-VG/TRYG-DKDC1ALKAPODB005-T0-TRGT-V15"

echo "Copying source volume 16 snapshot onto cloned Target volume 16"

su - ecptia -c "ssh ${User}@${Array} purevol copy --overwrite ${SrcPG}.DM1-$SUFFIX.OS-TRYG-TIA-SNAP-SRC-VG/TRYG-DKDC1ALKAPODB003-T0-V16 OS-TRYG-TIA-SNAP-TRGT-VG/TRYG-DKDC1ALKAPODB005-T0-TRGT-V16"

echo "Copying source volume 17 snapshot onto cloned Target volume 17"

su - ecptia -c "ssh ${User}@${Array} purevol copy --overwrite ${SrcPG}.DM1-$SUFFIX.OS-TRYG-TIA-SNAP-SRC-VG/TRYG-DKDC1ALKAPODB003-T0-V17 OS-TRYG-TIA-SNAP-TRGT-VG/TRYG-DKDC1ALKAPODB005-T0-TRGT-V17"

echo "Copying source volume 18 snapshot onto cloned Target volume 18"

su - ecptia -c "ssh ${User}@${Array} purevol copy --overwrite ${SrcPG}.DM1-$SUFFIX.OS-TRYG-TIA-SNAP-SRC-VG/TRYG-DKDC1ALKAPODB003-T0-V18 OS-TRYG-TIA-SNAP-TRGT-VG/TRYG-DKDC1ALKAPODB005-T0-TRGT-V18"

echo "Copying source volume 19 snapshot onto cloned Target volume 19"

su - ecptia -c "ssh ${User}@${Array} purevol copy --overwrite ${SrcPG}.DM1-$SUFFIX.OS-TRYG-TIA-SNAP-SRC-VG/TRYG-DKDC1ALKAPODB003-T0-V19 OS-TRYG-TIA-SNAP-TRGT-VG/TRYG-DKDC1ALKAPODB005-T0-TRGT-V19"

echo "Copying source volume 20 snapshot onto cloned Target volume 20"

su - ecptia -c "ssh ${User}@${Array} purevol copy --overwrite ${SrcPG}.DM1-$SUFFIX.OS-TRYG-TIA-SNAP-SRC-VG/TRYG-DKDC1ALKAPODB003-T0-V20 OS-TRYG-TIA-SNAP-TRGT-VG/TRYG-DKDC1ALKAPODB005-T0-TRGT-V20"


}

###############################MOUNTING_FILESYSTEM######################################

mountfs() {
#Replace the following with relevant mount details for your environment
mount /data/ora_data21
mount /data/ora_temp21
mount /data/ora_arch21
mount /data/ora_redo21
mount /data/ora_redo22
}
echo "Taking snapshot of volumes" > $LOGFILE
puresnap >> $LOGFILE
if [ $? -ne 0 ]
then
echo "Could not Execute Snapshot Creation Script successfully" >> $LOGFILE
echo "Snapshot Creation Script executed with Error.\n\nHence Snap cloning failed at `date '+%d'-'%m'-'%y','%H':'%M'`"|mailx -s "Snap Cloning for TIARAP Status" -r alkapodb005@tryg.dk tryg.unix.dist@tryg.dk zennith.devish@tryg.dk suvajit.maity@tryg.dk saroj.biswal@tryg.dk milan.sarkar@tryg.dk satadru.das@tryg.dk cloudops.storage@tcs.com prateek.sobti@tcs.com
exit 0
else
echo "Snapshot Mount Script executed successfully" >> $LOGFILE
su - oracle -c "/opt/oracle/TCS/script/shut_oldDB.sh" >> $LOGFILE
if [ $? -ne 0 ]
then
echo "Could not Bring down the Database successfully" >> $LOGFILE
echo "Could not bring down the TIARAP Dataibase.\n\nHence Snap cloning failed at `date '+%d'-'%m'-'%y','%H':'%M'`"|mailx -s "Snap Cloning for TIARAP Status" -r alkapodb005@tryg.dk tryg.unix.dist@tryg.dk zennith.devish@tryg.dk suvajit.maity@tryg.dk saroj.biswal@tryg.dk milan.sarkar@tryg.dk satadru.das@tryg.dk
exit 0
else
echo "Database Brought down successfully" >> $LOGFILE
#sdate=$(date +"%s")
echo "Unmounting filesystems...." >> $LOGFILE
unmountfs >> $LOGFILE
if [ $? -ne 0 ]
then
echo "Could not Unmount filesystem successfully" >> $LOGFILE
echo "Could not Unmount filesystem.\n\nHence Snap cloning failed at `date '+%d'-'%m'-'%y','%H':'%M'`"|mailx -s "Snap Cloning for TIARAP Status" -r alkapodb005@tryg.dk tryg.unix.dist@tryg.dk zennith.devish@tryg.dk suvajit.maity@tryg.dk saroj.biswal@tryg.dk milan.sarkar@tryg.dk satadru.das@tryg.dk
exit 0
else
echo "Filesystem Unmounted successfully" >> $LOGFILE
echo "Taking snapshot of volumes" >> $LOGFILE
mountsnap >> $LOGFILE
if [ $? -ne 0 ]
then
echo "Could not mount Snapshot successfully" >> $LOGFILE
echo "Snapshot Mount executed with Error.\n\nHence Snap cloning failed at `date '+%d'-'%m'-'%y','%H':'%M'`"|mailx -s "Snap Cloning for TIARAP Status" -r alkapodb005@tryg.dk tryg.unix.dist@tryg.dk zennith.devish@tryg.dk suvajit.maity@tryg.dk saroj.biswal@tryg.dk milan.sarkar@tryg.dk satadru.das@tryg.dk cloudops.storage@tcs.com prateek.sobti@tcs.com
exit 0
else
echo "Mounted Snapshot successfully" >> $LOGFILE
echo "Mounting filesystems ..." >> $LOGFILE
mountfs >> $LOGFILE
if [ $? -ne 0 ]
then
echo "Could not mount Filesystem successfully" >> $LOGFILE
echo "Could not mount Filesystems.\n\nHence Snap cloning failed at `date '+%d'-'%m'-'%y','%H':'%M'`"|mailx -s "Snap Cloning for TIARAP Status" -r alkapodb005@tryg.dk tryg.unix.dist@tryg.dk zennith.devish@tryg.dk suvajit.maity@tryg.dk saroj.biswal@tryg.dk milan.sarkar@tryg.dk satadru.das@tryg.dk
exit 0
else
echo "Mounted Filesystem successfully" >> $LOGFILE
echo "Changing Ownership of DB Filesystem" >> $LOGFILE
chown -R oracle:dba /data/ora_* >> $LOGFILE
###########################################STARTING_DATABASE##############################
echo "TIAPROD Database is starting" >> $LOGFILE
su - oracle -c "/opt/oracle/TCS/script/start_Tiaprod.sh" >> $LOGFILE
if [ $? -ne 0 ]
then
echo "Could not start Database_TIAPROD successfully" >> $LOGFILE
echo "Could not start Database_TIAPROD.\n\nHence Snap cloning failed at `date '+%d'-'%m'-'%y','%H':'%M'`"|mailx -s "Snap Cloning for TIARAP Status" -r alkapodb005@tryg.dk tryg.unix.dist@tryg.dk zennith.devish@tryg.dk suvajit.maity@tryg.dk saroj.biswal@tryg.dk milan.sarkar@tryg.dk satadru.das@tryg.dk
exit 0
else
echo "TIAPROD Database Started successfully" >> $LOGFILE
echo "TIARAP Database is starting" >> $LOGFILE
su - oracle -c "/opt/oracle/TCS/script/start_Tiarap_DBIdChng.sh" >> $LOGFILE
if [ $? -ne 0 ]
then
echo "Could not start Database_TIARAP successfully" >> $LOGFILE
echo "Could not start Database_TIARAP.\n\nHence Snap cloning failed at `date '+%d'-'%m'-'%y','%H':'%M'`"|mailx -s "Snap Cloning for TIARAP Status" -r alkapodb005@tryg.dk tryg.unix.dist@tryg.dk zennith.devish@tryg.dk suvajit.maity@tryg.dk saroj.biswal@tryg.dk milan.sarkar@tryg.dk satadru.das@tryg.dk
exit 0
else
echo "Database TIARAP started successfully." >> $LOGFILE
echo "Executing the Custom Script" >> $LOGFILE
su - oracle -c "/opt/oracle/TCS/script/run_Tiarap_custom.sh" >> $LOGFILE 2>&1
if [ $? -ne 0 ]
then
echo "Custom post SQL script executed with error" >> $LOGFILE
echo "Custom post SQL Script executed with Error for TIARAP at `date '+%d'-'%m'-'%y','%H':'%M'`"|mailx -s "Snap Cloning for TIARAP Status" -r alkapodb005.alkait.net@tryg.dk tryg.unix.dist@tryg.dk zennith.devish@tryg.dk suvajit.maity@tryg.dk saroj.biswal@tryg.dk milan.sarkar@tryg.dk satadru.das@tryg.dk
exit 0
else
echo "Custom post SQL script executed successfully" >> $LOGFILE
echo "Copying TIA Data" >> $LOGFILE
ssh tcs-adm@alkapodb003 'sudo -u tia ksh -c "/usr/bin/cp -IRp /TSFAprod/tia50code/tiaenv/tiaprod/* /home/tia/TSFA/tiamigr/mig_tia50code/tiaenv/tiarap/"'
if [ $? -ne 0 ]
then
echo "TIA Data Copy encountered an Error" >> $LOGFILE
echo "TIA Data Copy encountered an Error at `date '+%d'-'%m'-'%y','%H':'%M'`"|mailx -s "Snap Cloning for TIARAP Status" -r alkapodb005.alkait.net@tryg.dk tryg.unix.dist@tryg.dk zennith.devish@tryg.dk suvajit.maity@tryg.dk saroj.biswal@tryg.dk milan.sarkar@tryg.dk satadru.das@tryg.dk
exit 0
else
ssh tcs-adm@alkapodb003 'sudo touch /home/tia/TSFA/tiamigr/mig_tia50code/tiaenv/tiarap/TIARAP.OK'
echo "TIA Data Copy completed successfully" >> $LOGFILE
fi
fi
fi
fi
fi
fi
fi
fi
fi
echo "Databases TIARAP Cloned successfully at `date '+%d'-'%m'-'%y','%H':'%M'`"|mailx -s "Snap Cloning for TIARAP Status" -r alkapodb005@tryg.dk zennith.devish@tryg.dk suvajit.maity@tryg.dk saroj.biswal@tryg.dk milan.sarkar@tryg.dk satadru.das@tryg.dk michael.egemar.christensen@tryg.dk

#! /usr/bin/env bash

# Basic info
ROOT=`df -Ph | grep xvda1 | awk '{print $4}' | tr -d '\n'`

# System load
LOAD1=`cat /proc/loadavg | awk {'print $1'}`
LOAD5=`cat /proc/loadavg | awk {'print $2'}`
LOAD15=`cat /proc/loadavg | awk {'print $3'}`
VCPU=`lscpu|awk "NR == 4"|awk '{print $2}'`
SOCKET=`lscpu|awk "NR == 8"|awk '{print $2}'`
MEM_TOTAL=`free -m|grep Mem|awk '{print $2}'`
MEM_AVAIL=`free -m|grep Mem|awk '{print $7}'`
MEM_USED=`expr $MEM_TOTAL - $MEM_AVAIL`
SWAP_TOTAL=`free -m|grep Swap|awk '{print $2}'`
SWAP_USED=`free -m|grep Swap|awk '{print $3}'`
ROOT_TOTAL=`df -Th /|tail -1|awk '{print $3}'|cut -d'G' -f1`
ROOT_USAGE=`df -Th /|tail -1|awk '{print $6}'`
USER_LOG=`who|wc -l`
KERN=`uname -r`
KERN_LAT=`rpm -q kernel -last|head -n 1|awk '{print $1}'|sed 's/kernel-//'`


echo "
****************************************************************************

Welcome to `cat /etc/redhat-release`

  System information as of `date`

  Hostname: 			`hostname -s`
  System Load:			$LOAD1, $LOAD5, $LOAD15 (1, 5, 15 min)
  vCPU:				$VCPU
  Socket:			$SOCKET
  Memory Usage:			$MEM_USED / $MEM_TOTAL MB
  Swap Usage:			$SWAP_USED / $SWAP_TOTAL MB
  Kernel:			$KERN
  Usage of /:			$ROOT_USAGE of $ROOT_TOTAL GB
  Users logged in:		$USER_LOG"

for E_PORT in `ip r|tail -n +2|awk '{print $3}'|grep -vE "docker|br-"`
do
LENGTH=`echo $E_PORT|wc -c`
if [ $LENGTH -gt 6 ]
then
echo -e "  IP address for $E_PORT:\t`ip a show dev $E_PORT|grep -w inet|awk '{print $2}'|cut -d'/' -f1`"
else
echo -e "  IP address for $E_PORT:\t\t`ip a show dev $E_PORT|grep -w inet|awk '{print $2}'|cut -d'/' -f1`"
fi
done

echo -e "  Security Patches Available:\t`cat /var/tmp/sec-patch`

****************************************************************************"

if [ $KERN != $KERN_LAT ]
then
echo -e "
\e[35m***** A Reboot is pending for the server *****

\e[39m****************************************************************************"
fi

s=0
for size in `fdisk -l 2> /dev/null|grep /dev/sd|grep bytes|awk '{print $5}'`
do
s=`expr $s + $size`
done
s=`expr $s / 1073741824`
echo "HOSTNAME = `hostname`"
echo "TOTAL STORAGE SPACE = $s GB"
SWAP=`swapon -s |grep dm|awk '{sum+=$3}END{print sum}'`
FS=`df -PTk|grep ext4|awk '{sum+=$4}END{print sum}'`
USED_K=`expr $SWAP + $FS`
USED_G=`expr $USED_K / 1048576`
echo "TOTAL USED SPACE = $USED_G GB"
FREE=`expr $s - $USED_G`
echo "TOTAL FREE SPACE = $FREE GB"

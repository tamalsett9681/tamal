E_PORT=`ip r|tail -n +2|awk '{print $3}'|grep -vE "docker|br-"|sort -n|head -1`
echo S@01TcS3cP@tRyG631 | realm join --computer-ou='OU=TCSECP,OU=ECPServers,OU=Test,OU=TCS,DC=prd1,DC=prdroot,DC=net' --user sa01.tcsecp prd1.prdroot.net --install=/ 
if [ -f /etc/sssd/sssd.conf ]
then
/bin/sed -i '16s/.*/use_fully_qualified_names = False/' /etc/sssd/sssd.conf
/bin/sed -i '17s/.*/fallback_homedir = \/home\/%u/' /etc/sssd/sssd.conf
/bin/sed -i 's/cache_credentials = True/cache_credentials = False/g' /etc/sssd/sssd.conf
/bin/sed -i '14i\krb5_validate = False' /etc/sssd/sssd.conf
/bin/echo "ldap_use_tokengroups = false" >> /etc/sssd/sssd.conf
/bin/echo "dyndns_update = True" >> /etc/sssd/sssd.conf
/bin/echo "dyndns_update_ptr = True" >> /etc/sssd/sssd.conf
/bin/echo "dyndns_refresh_interval = 43200" >> /etc/sssd/sssd.conf
/bin/echo "dyndns_ttl = 3600" >> /etc/sssd/sssd.conf
/bin/echo "dyndns_iface = $E_PORT" >> /etc/sssd/sssd.conf
/bin/echo "ad_hostname = `hostname`.prd1.prdroot.net" >> /etc/sssd/sssd.conf
mkdir /etc/systemd/system/sssd.service.d
/bin/wget -q -O /etc/systemd/system/sssd.service.d/network.conf http://10.48.64.61/network.conf
/bin/systemctl daemon-reload
/bin/systemctl reset-failed sssd
/bin/systemctl restart sssd
fi
rm -rf $0

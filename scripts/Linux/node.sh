unset http_proxy
wget -r -np -nH -R "index.html*" -R "*.gif" -P /opt -R "node_exporter?*" http://10.48.64.61/node_exporter
wget -q -O /etc/systemd/system/node_exporter.service http://10.48.64.61/node_exporter.service
chmod +x /opt/node_exporter/node_exporter
systemctl daemon-reload
systemctl enable --now node_exporter
rm -rf $0

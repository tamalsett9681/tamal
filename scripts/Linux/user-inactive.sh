#!/bin/bash

# Modifying account inactive days to 90 for the non-system accounts
for user in `cat /etc/passwd|awk -F: '{if ($3 >= 1000) print $1}'|grep -v nfsnobody`
do
chage -I $user
done

# Modifying default account inactive days to 90
sed -i 's/INACTIVE=.*/INACTIVE=90/g' /etc/default/useradd

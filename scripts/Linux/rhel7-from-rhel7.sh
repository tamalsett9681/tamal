#!/bin/bash

LOGFILE=/var/log/reposync/rhel-7-server-rpms/reposync-`date '+%d%m%Y'`.log

reposync --gpgcheck -l --repoid=rhel-7-server-rpms --download_path=/var/www/html --downloadcomps --download-metadata > $LOGFILE 2>&1
reposync --gpgcheck -l --repoid=rhel-7-server-extras-rpms --download_path=/var/www/html --downloadcomps --download-metadata >> $LOGFILE 2>&1
reposync --gpgcheck -l --repoid=rhel-7-server-optional-rpms --download_path=/var/www/html --downloadcomps --download-metadata >> $LOGFILE 2>&1
createrepo -v /var/www/html/rhel-7-server-rpms -g /var/www/html/rhel-7-server-rpms/comps.xml >> $LOGFILE 2>&1
createrepo -v /var/www/html/rhel-7-server-extras-rpms -g /var/www/html/rhel-7-server-extras-rpms/comps.xml >> $LOGFILE 2>&1
createrepo -v /var/www/html/rhel-7-server-optional-rpms -g /var/www/html/rhel-7-server-optional-rpms/comps.xml >> $LOGFILE 2>&1
yum clean all >> $LOGFILE 2>&1
yum list-sec >> $LOGFILE 2>&1
mv /var/cache/yum/x86_64/7Server/rhel-7-server-rpms/gen/updateinfo.xml /var/www/html/rhel-7-server-rpms/repodata/updateinfo.xml >> $LOGFILE 2>&1
mv /var/cache/yum/x86_64/7Server/rhel-7-server-extras-rpms/gen/updateinfo.xml /var/www/html/rhel-7-server-extras-rpms/repodata/updateinfo.xml >> $LOGFILE 2>&1
mv /var/cache/yum/x86_64/7Server/rhel-7-server-optional-rpms/gen/updateinfo.xml /var/www/html/rhel-7-server-optional-rpms/repodata/updateinfo.xml >> $LOGFILE 2>&1
modifyrepo /var/www/html/rhel-7-server-rpms/repodata/updateinfo.xml /var/www/html/rhel-7-server-rpms/repodata >> $LOGFILE 2>&1
modifyrepo /var/www/html/rhel-7-server-extras-rpms/repodata/updateinfo.xml /var/www/html/rhel-7-server-extras-rpms/repodata >> $LOGFILE 2>&1
modifyrepo /var/www/html/rhel-7-server-optional-rpms/repodata/updateinfo.xml /var/www/html/rhel-7-server-optional-rpms/repodata >> $LOGFILE 2>&1

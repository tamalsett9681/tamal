echo '015@Pc#5CtHcM@21' | realm join --computer-ou='OU=HCM_AutoDeploy,OU=Servers,OU=TCS,DC=prddmz,DC=local' --user sa01.tcsecp prddmz.local --install=/ 
if [ -f /etc/sssd/sssd.conf ]
then
/bin/sed -i '16s/.*/use_fully_qualified_names = False/' /etc/sssd/sssd.conf
/bin/sed -i '17s/.*/fallback_homedir = \/home\/%u/' /etc/sssd/sssd.conf
/bin/sed -i 's/cache_credentials = True/cache_credentials = False/g' /etc/sssd/sssd.conf
/bin/sed -i '14i\krb5_validate = False' /etc/sssd/sssd.conf
/bin/echo "dyndns_update = True" >> /etc/sssd/sssd.conf
/bin/echo "dyndns_refresh_interval = 43200" >> /etc/sssd/sssd.conf
/bin/echo "dyndns_ttl = 3600" >> /etc/sssd/sssd.conf
/bin/echo "dyndns_iface = ens192" >> /etc/sssd/sssd.conf
/bin/echo "ad_hostname = `hostname`.prddmz.local" >> /etc/sssd/sssd.conf
/bin/systemctl reset-failed sssd
/bin/systemctl restart sssd
fi
rm -rf $0

#!/bin/bash

LOGFILE=/var/log/reposync/rhel-6-server-rpms/reposync-`date '+%d%m%Y'`.log

echo `ls /etc/pki/entitlement/*-key.pem` > /etc/yum/vars/clientkey
echo `ls /etc/pki/entitlement/*.pem|grep -v key` > /etc/yum/vars/clientcert
reposync --config=/etc/reposync.conf -p /var/www/html --download-metadata --repo=rhel-6-server-rpms > $LOGFILE 2>&1

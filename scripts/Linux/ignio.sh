for host in `ls /sys/class/scsi_host/|xargs`
do
echo "- - -" > /sys/class/scsi_host/$host/scan
done
if [ -b /dev/sdb ]
then
sed -e 's/\s*\([\+0-9a-zA-Z]*\).*/\1/' << EOF | fdisk /dev/sdb
n
p
1


t
8e
p
w
EOF
partprobe /dev/sdb
pvcreate /dev/sdb1
vgcreate igniovg /dev/sdb1
lvcreate -l +100%Free -n igniolv igniovg
mkfs -t ext4 /dev/mapper/igniovg-igniolv
if [ ! -d /ignio ]
then
mkdir /ignio
fi
uuid=`blkid /dev/mapper/igniovg-igniolv|cut -d' ' -f2`
echo "$uuid /ignio ext4 defaults 0 0" >> /etc/fstab
mount -a
chown ignio:ignio /ignio
fi

rm -rf `hostname -s`_errpt.out
#touch `hostname`_errpt.out
export LANG=en_US.UTF-8
tday=`date '+%m%d%H%M%y'`
n=$(date +%u)
if [ "$n" -eq 1 ]
then yday=`perl -MPOSIX -e 'print strftime("%m%d%H%M%y",localtime(time-86400*3))'`
else
yday=`perl -MPOSIX -e 'print strftime("%m%d%H%M%y",localtime(time-86400*1))'`
fi
/usr/bin/errpt -s $yday -e $tday  >> `hostname -s`_errpt.out
m=$(cat `hostname -s`_errpt.out|wc -l)
if [ "$m" -eq 0 ]
then echo "Current date is `date`" >> `hostname -s`_errpt.out
echo "No recent error found." >> `hostname -s`_errpt.out
fi

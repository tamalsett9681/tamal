#!/bin/bash

LOGFILE=/var/log/reposync/rhel-8-server-rpms/reposync-`date '+%d%m%Y'`.log

reposync -p /var/www/html/rhel-8-server-rpms --download-metadata --repo=rhel-8-for-x86_64-baseos-rpms > $LOGFILE 2>&1
reposync -p /var/www/html/rhel-8-server-rpms --download-metadata --repo=rhel-8-for-x86_64-appstream-rpms >> $LOGFILE 2>&1
reposync -p /var/www/html/rhel-8-server-rpms --download-metadata --repo=codeready-builder-for-rhel-8-x86_64-rpms >> $LOGFILE 2>&1

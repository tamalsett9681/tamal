s=0
for size in `fdisk -l|grep /dev/sd|grep bytes|awk '{print $5}'`
do
s=`expr $s + $size`
done
s=`expr $s / 1073741824`
echo "HOSTNAME = `hostname`"
echo "TOTAL STORAGE SPACE = $s GB"

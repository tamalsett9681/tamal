++++++++=================LDAP Server Configuration++++++++++++++++++++===================

yum install openldap openldap-clients openldap-servers

chown -R ldap:ldap /var/lib/ldap

vim  /etc/openldap/slapd.d/cn=config.ldif

olcAllows: bind_v2 update_anon
olcConnMaxPending: 100
olcConnMaxPendingAuth: 1000
olcDisallows: bind_anon
olcIdleTimeout: 180
olcLogFile: /var/log/slapd.log
olcReferral: ldap://root.openldap.org
olcWriteTimeout: 180




vim /etc/openldap/slapd.d/cn=config/olcDatabase={2}bdb.ldif
olcReadOnly: boolean
olcReadOnly: TRUE
olcRootDN: cn=root,dc=example,dc=com
olcRootPW: {SSHA}WczWsyPEnMchFf1GRTweq2q7XJcvmSxD
olcSuffix: dc=example,dc=com

service slapd start


useradd ldapuser1
passwd ldapuser1

useradd ldapuser2
passwd ldapuser2

useradd ldapuser3
passwd ldapuser3

useradd ldapuser4
passwd ldapuser4




yum install migrationtools

/usr/share/migrationtools



vim /usr/share/migrationtools/migrate_common.ph


$DEFAULT_MAIL_DOMAIN = "example.com";
$DEFAULT_BASE = "dc=example,dc=com";
extended_schema	= 1
ou="Groups"

./migrate_base.pl >/root/base.ldif





getent passwd |tail -n 4
getent passwd |tail -n 4 >/root/users
getent shadow |tail -n 4
getent shadow |tail -n 4 >/root/passwords
getent group |tail -n 4
getent group |tail -n 4 >/root/group

vim migrate_passwd.pl
change -- "/etc/shadow" ==== > "/root/paswords"

 ./migrate_passwd.pl /root/users
 ./migrate_passwd.pl /root/users >/root/users.ldif

./migrate_group.pl /root/group
./migrate_group.pl /root/group >/root/group.ldif


ldapadd -x -W -D "cn=root,dc=example,dc=com" -f /root/base.ldif
ldapadd -x -W -D "cn=root,dc=example,dc=com" -f /root/users.ldif
ldapadd -x -W -D "cn=root,dc=example,dc=com" -f /root/group.ldif

ldapsearch -x -b 'dc=example,dc=com' '(objectclass=*)'

=========================
LDAP Client  :--

 yum install openldap openldap-clients nss_ldap sssd
 yum install pam_ldap
 yum install authconfig


vim /etc/sssd/sssd.conf

add the following entry

ldap_uri = ldap://10.59.20.42/
ldap_id_use_start_tls = False
ldap_tls_cacertdir = /etc/openldap/cacerts
ldap_default_authtok_type = password
ldap_default_authtok = YOUR_PASSWORD
cache_credentials = True
enumerate=true
[sssd]
services = nss, pam, autofs

vi /etc/nsswitch.conf

add the following lines

passwd:     files sss
shadow:     files sss
group:      files sss

service sssd restart

vim /etc/pam.d/password-auth

add the following entries..

auth 	   sufficient   pam_ldap.so
account    sufficient   pam_ldap.so
session    sufficient   pam_ldap.so
password   sufficient   pam_ldap.so

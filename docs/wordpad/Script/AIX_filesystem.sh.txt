#!/bin/ksh
m=0
n=0
for i in `df -gt|tail -n +2|grep -v "/proc"|awk '{print $5}'|cut -d% -f1`
do n=`expr $n + 1`
if [ "$i" -ge 91 ]
then
m=`expr $m + 1`
if [ "$m" -eq 1 ]
then echo "Dear Team,\n\nPlease log a ticket against the below Problem.\n\nServer Name: trfsapsmp01\nProblem: Filesystem Utilization > 90%\n\n" > /home/tatatas/filesystem.txt
echo "The following filesystems are more than 90% utilized:" >> /home/tatatas/filesystem.txt
df -gt|head -n 1 >> /home/tatatas/filesystem.txt
fi
df -gt|tail -n +2|grep -v "/proc"|awk "NR==$n" >> /home/tatatas/filesystem.txt
fi
done
if [ "$m" -ne 0 ]
then echo "\n\n\n\nNote: This is an auto generated mail. Please do not reply." >> /home/tatatas/filesystem.txt
cat /home/tatatas/filesystem.txt | mail -s "Filesystem Utilization > 90% for trfsapsmp01" -r trfsapsmp01@tryg.dk -c tryg.unix@tcs.com tryg.sd@atea.dk
rm -rf /home/tatatas/filesystem.txt
fi

#!/bin/sh

# Simple script to obtain host info from AIX systems
# Script is divided into sections to match discovery methods

os=`uname -s`
if [ "$os" != "AIX" ]; then
    echo This script must be run on AIX
    exit 1
fi

# Set PATH
PATH=/bin:/usr/bin:/usr/sbin
export PATH

# Initialisation

tw_locale=`locale -a | grep -i en_us | grep -i "utf.*8" | head -n 1 2>/dev/null`

LANGUAGE=""
if [ "$tw_locale" != "" ]; then
    LANG=$tw_locale
    LC_ALL=$tw_locale
else
    LANG=C
    LC_ALL=C
fi
export LANG LC_ALL


# Stop alias commands changing behaviour.
unalias -a

# Insulate against systems with -u set by default.
set +u

if [ -w /tmp ] 
then
    # use a /tmp file to capture stderr
    TW_CAPTURE_FILE=/tmp/tideway_status_$$
    export TW_CAPTURE_FILE
    rm -f $TW_CAPTURE_FILE

    tw_capture(){
        TW_NAME=$1
        shift
        echo begin cmd_status_err_$TW_NAME >>$TW_CAPTURE_FILE
        "$@" 2>>$TW_CAPTURE_FILE
        RETURN_VAL=$?
        echo end cmd_status_err_$TW_NAME >>$TW_CAPTURE_FILE

        echo cmd_status_$TW_NAME=$RETURN_VAL >>$TW_CAPTURE_FILE
        return $RETURN_VAL
    }

    tw_report(){
        if [ -f $TW_CAPTURE_FILE ]
        then 
            cat $TW_CAPTURE_FILE 2>/dev/null
            rm -f $TW_CAPTURE_FILE 2>/dev/null
        fi
    }
else
    # can't write to /tmp - do not capture anything
    tw_capture(){
        shift
        "$@" 2>/dev/null
    }

    tw_report(){
        echo "cmd_status_err_status_unavailable=Unable to write to /tmp"
    }
fi 

# replace the following PRIV_XXX functions with one that has the path to a
# program to run the commands as super user, e.g. sudo. For example
# PRIV_LSOF() {
#   /usr/bin/sudo "$@"
# }

# lsof requires superuser privileges to display information on processes
# other than those running as the current user
PRIV_LSOF() {
  "$@"
}

# This function supports running privileged commands from patterns
PRIV_RUNCMD() {
  "$@"
}

# This function supports privileged cat of files.
# Used in patterns and to get file content.
PRIV_CAT() {
  cat "$@"
}

# This function supports privilege testing of attributes of files.
# Used in conjunction with PRIV_CAT and PRIV_LS
PRIV_TEST() {
  test "$@"
}

# This function supports privilege listing of files and directories
# Used in conjunction with PRIV_TEST 
PRIV_LS() {
  ls "$@"
}

# This function supports privilege listing of file systems and related
# size and usage.
PRIV_DF() {
  "$@"
}

# lslpp requires superuser privileges to list all installed packages
PRIV_LSLPP() {
    "$@"
}

# lswpar requires superuser privileges to get wpar information.
PRIV_LSWPAR() {
    "$@"
}


# Formatting directive
echo FORMAT AIX

# getDeviceInfo
echo --- START device_info

ihn=`hostname 2>/dev/null`
echo 'hostname:' $ihn
if [ -r /etc/resolv.conf ]; then
    echo 'dns_domain:' `awk '/^(domain|search)/ { print $2; exit }' /etc/resolv.conf 2>/dev/null`
fi
echo 'domain:' `domainname 2>/dev/null`
# VIO support
if [ -r /usr/ios/cli/ios.level ]; then
    viover=`cat /usr/ios/cli/ios.level`
    os="VIO $viover"
else
    osver=`oslevel 2>/dev/null`
    if [ $? -ne 0 ]; then
        osver=""
    fi
    if [ "$osver" = "" -o "$osver" = "TYPERML" ]; then
        osmaj=`uname -v 2>/dev/null`
        osmin=`uname -r 2>/dev/null`
        osver="$osmaj.$osmin"
    fi
    os="`uname -s 2>/dev/null` $osver"
fi
echo 'os:' $os
maintlevel=`oslevel -r 2>/dev/null`
if [ $? -ne 0 ]; then
    maintlevel=""
fi
if [ "$maintlevel" != "" ]; then
    echo 'os_level:' $maintlevel
fi
echo 'os_arch:' `uname -p 2>/dev/null`

echo --- END device_info

# getHostInfo
echo --- START host_info

/usr/bin/lparstat -i > /tmp/tideway.$$ 2>/dev/null

echo 'begin lparstat:'
cat /tmp/tideway.$$
echo 'end lparstat'

# Extract CPU info from lparstat
num_logical_processors=`egrep '^Online Virtual CPUs' /tmp/tideway.$$ | cut -f2 -d:`

rm -f /tmp/tideway.$$

wparid=0
if [ `uname -v` -ge 6 ]; then
    # AIX 6 WPAR support
    wparid=`uname -W 2>/dev/null`
    echo "wparid: $wparid"
    if [ $wparid -eq 0 ]; then
        echo 'begin lswpar:'
        PRIV_LSWPAR /usr/sbin/lswpar -cqa name,state,type,hostname,directory,key,owner,application 2>/dev/null
        echo 'end lswpar'
    fi
fi
if [ -x /usr/sbin/prtconf ]; then
    /usr/sbin/prtconf > /tmp/tideway.$$ 2>/dev/null
    echo 'model:' `egrep '^System Model:' /tmp/tideway.$$ | cut -f2 -d: | sed -e 's/IBM,//'`
    echo 'kernel:' `egrep '^Kernel Type:' /tmp/tideway.$$ | cut -f2 -d:`
    if [ $wparid -eq 0 ]; then
        serial=`egrep '^Machine Serial Number:' /tmp/tideway.$$ | cut -f2 -d:`
        # Don't output serial number if it isn't available. Also work around
        # bug in prtconf on AIX 5.3 where there's no newline after the serial
        # number output if it isn't available.
        if [ "$serial" != "" -a  "$serial" != " Not Available" -a "$serial" != " Not AvailableProcessor Type" ]; then
          echo 'candidate_serial[]:' $serial
        else
          lsattr -E -l sys0 -a systemid 2>/dev/null | awk '{print "candidate_serial[]: " substr($2,7);}'
        fi
        
        # Use this processor value if lparstat failed
        if [ "$num_logical_processors" = "" ]; then
            num_logical_processors=`egrep '^Number Of Processors:' /tmp/tideway.$$ | cut -f2 -d:`
        fi
        
        # Work around prtconf bug again
        cputype=`egrep '^(Machine Serial Number: Not Available)?Processor Type:' /tmp/tideway.$$ | sed -e 's/Machine Serial Number: Not Available//' | cut -f2 -d: | sed -e 's/_/ /'`
        
        cpuspeed=`egrep '^Processor Clock Speed:' /tmp/tideway.$$ | cut -f2 -d:`
    fi
    rm -f /tmp/tideway.$$
else
    lsattr -E -l sys0 -a modelname 2>/dev/null | awk '{print "model: " $2;}'
    lsattr -E -l sys0 -a systemid 2>/dev/null | awk '{print "candidate_serial[]: " substr($2,7);}'
    # Use this processor value if lparstat failed
    if [ "$num_logical_processors" = "" ]; then
        num_logical_processors=`lscfg -lproc\* 2>/dev/null | wc -l`
    fi
    cputype=`lsattr -E -l proc0 -a type 2>/dev/null | awk '{print $2;}' | sed -e 's/_/ /'`
    hz=`lsattr -E -l proc0 -a frequency 2>/dev/null | awk '{print $2;}'`
    cpuspeed=`expr $hz / 1000000 2>/dev/null`
    cpuspeed="$cpuspeed MHz"
fi

# Get candidate serial using lscfg -vp in case the above commands do not return any serial value
lscfg -vp 2>/dev/null | grep -p "System VPD:" | grep "Cabinet Serial No" | sed -e 's/.*Serial No\.*\(.*\)/candidate_serial[]: \1/'

if [ $wparid -eq 0 ]; then
    # Get SMT information
    smt_enabled=`lsattr -E -l proc0 -a smt_enabled 2>/dev/null | awk '{print $2;}'`
    echo "cpu_threading_enabled:" $smt_enabled
fi

if [ "$num_logical_processors" != "" ]; then
    echo "num_logical_processors:" $num_logical_processors
fi
if [ "$cputype" != "" -a "$cpuspeed" != "" ]; then
    echo "processor_type:" $cputype $cpuspeed
    echo "processor_speed:" $cpuspeed
fi

lsattr -E -l sys0 -a realmem 2>/dev/null | awk '{print "ram: " $2 "KB";}'
if [ -x /usr/sbin/hostid ]; then
    echo 'hostid:' `/usr/sbin/hostid`
fi
echo 'systemid:' `uname -F 2>/dev/null`
echo 'vendor: IBM'

# Get uptime in days and seconds
uptime 2>/dev/null | awk '
{
  if ( $4 ~ /day/ ) { 
    print "uptime:", $3; 
    z = split($5,t,":"); 
    printf( "uptimeSeconds: %d\n", ($3 * 86400) + (t[1] * 3600) + (t[2] * 60) ); 
  } else { 
    print "uptime: 0"; z = split($3,t,":"); 
    print "uptimeSeconds:", (t[1] * 3600) + (t[2] * 60); 
  }
}'

echo --- END host_info

# getMACAddresses
echo --- START lsdev_lscfg_mac

for i in `lsdev -Cc adapter -F name:status | grep "^en"`
do
  adapter=`echo $i | cut -f1 -d:`
  status=`echo $i | cut -f2 -d:`

  if [ $status = "Available" ]; then
    echo Begin-interface: $adapter
    lscfg -vl $adapter 2>/dev/null
    echo $? > /dev/null # Prevent terminal issues
    echo End-interface: $adapter
  fi
done

echo --- END lsdev_lscfg_mac

echo --- START netstat_link_mac

netstat -ia | grep link

echo --- END netstat_link_mac

# getIPAddresses
echo --- START ifconfig_ip
wparaddrs=""
if [ `uname -v` -lt 5 ]; then
    for name in `netstat -i | cut -f 1 -d\  | grep -Ev '^Name|^lo0|^$' | sort -u`
    do
        ifconfig $name 2>/dev/null
    done
else
    if [ `uname -v` -ge 6 ]; then
        wparaddrs=`PRIV_LSWPAR /usr/sbin/lswpar -Nqa address 2>/dev/null | xargs echo | sed -e 's/\./\./g
' -e 's/ /|/g'`
    fi
    if [ "$wparaddrs" = "" ]; then
        ifconfig -a 2>/dev/null
    else
        ifconfig -a 2>/dev/null | egrep -v "$wparaddrs"
    fi
fi

echo --- END ifconfig_ip

# getNetworkInterfaces
echo --- START ifdetails

echo begin lscfg:
for i in `lsdev -Cc adapter -F name:status | grep "^en"`
do
  adapter=`echo $i | cut -f1 -d:`
  status=`echo $i | cut -f2 -d:`

  if [ $status = "Available" ]; then
    echo Begin-interface: $adapter
    lscfg -vl $adapter 2>/dev/null
    echo $? > /dev/null # Prevent terminal issues
    echo End-interface: $adapter
  fi
done
echo end lscfg:
echo begin entstat:
for i in `lsdev -Cc adapter -F name:status | grep "^en"`
do
  adapter=`echo $i | cut -f1 -d:`
  status=`echo $i | cut -f2 -d:`

  if [ $status = "Available" ]; then
    echo Begin-interface: $adapter
    entstat -d $adapter 2>/dev/null
    echo End-interface: $adapter
  fi
done
echo end entstat:
# VIO support
if [ -x /usr/ios/cli/ioscli ]; then
  echo begin ioscli-lsmap-all:
  /usr/ios/cli/ioscli lsmap -all -net -fmt ":"
  echo end ioscli-lsmap-all:
fi

echo --- END ifdetails

echo --- START netstat_link_if

netstat -ia | grep link

echo --- END netstat_link_if

# getNetworkConnectionList
echo --- START netstat
if [ `uname -v` -ge 6 ]; then
    if [ `uname -W` -eq 0 ]; then
        netstat -an -f inet -@ 2>/dev/null | egrep "Global|Proto" | sed -e 's/Global //'
        netstat -an -f inet6 -@ 2>/dev/null | egrep "Global|Proto" | sed -e 's/Global //'
    else
        netstat -an -f inet 2>/dev/null
        netstat -an -f inet6 2>/dev/null
    fi
else
    netstat -an -f inet 2>/dev/null
    netstat -an -f inet6 2>/dev/null
fi

echo --- END netstat

# getProcessList
echo --- START ps
if [ `uname -v` -ge 6 ]; then
    ps -eo pid,ppid,uid,user,wpar,args | egrep "Global|PID" | sed -e 's/Global //'
else
    # pipe through cat to remove tty columns limit
    ps -eo pid,ppid,uid,user,args | cat
fi
# Using pe -eo still limits the length of the argument column
ps -ef | cat

echo --- END ps

# getPatchList
#   ** DISABLED **

# getProcessToConnectionMapping
echo --- START lsof-i
PRIV_LSOF lsof -l -n -P -F ptPTn -i 2>/dev/null
echo --- END lsof-i

# getPackageList
echo --- START lslpp
PRIV_LSLPP lslpp -l

echo --- END lslpp

# getHBAList
echo --- START hbainfo
for i in `lsdev -Cc adapter -F name:status:type | grep "^fcs"`
do
    adapter_name=`echo $i | cut -f1 -d:`
    adapter_status=`echo $i | cut -f2 -d:`
    adapter_type=`echo $i | cut -f3 -d: | sed -e 's/,/./'`
    if [ $adapter_status = "Available" ]; then
        echo begin lscfg-vl-$adapter_name:
        lscfg -vl $adapter_name
        echo $? > /dev/null # Prevent terminal issues
        echo end lscfg-vl-$adapter_name:
        echo begin lslpp-$adapter_name:
        lslpp -L devices.*.$adapter_type.*
        echo end lslpp-$adapter_name:
    fi
done
echo --- END hbainfo

# getServices
#   ** UNSUPPORTED **

# getFileSystems
echo --- START df
echo begin df:
if [ -x /usr/sysv/bin/df ]; then
    PRIV_DF /usr/sysv/bin/df -lg 2>/dev/null
#else
#    PRIV_DF df -k 2>/dev/null
fi
echo end df:
echo begin mount:
mount 2>/dev/null
echo end mount:
echo begin xtab:
if [ -r /etc/xtab ]; then
    cat /etc/xtab
fi
echo end xtab:
echo begin smbclient:
smbclient -N -L localhost
echo end smbclient:
echo begin smbconf:
configfile=`smbstatus -v 2>/dev/null | grep "using configfile" | awk '{print $4;}'`
if [ "$configfile" != "" ]; then
    if [ -r $configfile ]; then
        cat $configfile
    fi
fi
echo end smbconf:

echo --- END df


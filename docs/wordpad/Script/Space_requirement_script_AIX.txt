total=`df -gt|tail -n +2|grep -v -E "/proc|/aha|:"|awk '{sum+=$2} END {print sum}'|sed 's/,/./'`
free=`df -gt|tail -n +2|grep -v -E "/proc|/aha|:"|awk '{sum+=$4} END {print sum}'|sed 's/,/./'`
paging_mb=`lsps -a|tail -n +2|awk '{sum+=$4} END {print sum}'`
paging_gb=`echo "scale=2; $paging_mb/1024"|bc`
total1=`echo "scale=2; $total+$paging_gb"|bc`
echo "Total=$total1"
echo "Free=$free" 
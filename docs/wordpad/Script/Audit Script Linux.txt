echo ###############>> `hostname`.out
echo "HOSTNAME: `hostname`" >> `hostname`.out
echo ############### >> `hostname`.out
echo "\nDATE: "  >> `hostname`.out
date  >> `hostname`.out
echo ############### >> `hostname`.out
echo "\nPASSWORD POLICY (/etc/security/user)" >> `hostname`.out
cat /etc/login.defs >> `hostname`.out
echo ############### >> `hostname`.out
echo "\nSSHD_CONFIG" >> `hostname`.out
cat /etc/ssh/sshd_config  2>&1 >> `hostname`.out
echo ############### >> `hostname`.out
echo "\nSU LOG" >> `hostname`.out
cat /var/adm/sulog >> `hostname`.out
echo ############### >> `hostname`.out
echo "\nUSERS SULOGIN" >> `hostname`.out
for i in `lsuser ALL|awk '{print $1}'`
do lsuser -f $i|grep -Ew "^$i|su|sugroups" >> `hostname`.out
done
echo ############### >> `hostname`.out
echo "\nSUDO LOG" >> `hostname`.out
cat /opt/soe/local/etc/sudoers 2>&1 >> `hostname`.out
echo ############### >> `hostname`.out
echo "\nRLOGIN" >> `hostname`.out
for i in `lsuser ALL|awk '{print $1}'`
do lsuser -f $i|grep -Ew "^$i|rlogin" >> `hostname`.out
done
echo ############### >> `hostname`.out
echo "\nHOME_DIRECTORY" >> `hostname`.out
ls -lR /home 2>&1 >> `hostname`.out
echo ############### >> `hostname`.out
echo "\nWORLD-WRITABLE FILES" >> `hostname`.out
find / -perm -0002 -type f -ls 2>&1 >> `hostname`.out
echo ############### >> `hostname`.out
echo "\nUMASK" >> `hostname`.out
umask >> `hostname`.out
echo ############### >> `hostname`.out
echo "\nFAILED LOGIN" >> `hostname`.out
who -a /etc/security/failedlogin 2>&1 >> `hostname`.out
echo ############### >> `hostname`.out
echo "\nAUDIT" >> `hostname`.out
cat /etc/security/audit/bincmds >> `hostname`.out
audit query >> `hostname`.out
echo ############### >> `hostname`.out
echo "\nSERVICES" >> `hostname`.out
for srvs in bgssd bpcd bpjava-msvc cacluadm ftp ntalk login ntalk vnetd vopied wsmserver xmquery
do ps -ef|grep $srvs 2>&1 >> `hostname`.out
done
echo ############### >> `hostname`.out
echo "\nCRON.ALLOW" >> `hostname`.out
cat /var/adm/cron/cron.allow 2>&1 >> `hostname`.out
echo "\nCRON.DENY" >> `hostname`.out
cat /var/adm/cron/cron.deny 2>&1 >> `hostname`.out
echo "\nAT.ALLOW" >> `hostname`.out
cat /var/adm/cron/at.allow 2>&1 >> `hostname`.out
echo "\nAT.DENY" >> `hostname`.out
cat /var/adm/cron/at.deny 2>&1 >> `hostname`.out
echo ############### >> `hostname`.out
echo "\nNTP" >> `hostname`.out
lssrc -s xntpd >> `hostname`.out
ntpq -p 2>&1 >> `hostname`.out
echo ############### >> `hostname`.out
echo "\nLOGIN SHELL" >> `hostname`.out
cat /etc/passwd|cut -d: -f1,7 >> `hostname`.out
echo ############### >> `hostname`.out
cat `hostname`.out > `hostname`\_`date '+%d-%m-%Y_%H%M'`.out 
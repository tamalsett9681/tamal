#!/bin/sh

# Simple script to obtain host info from Linux systems
# Script is divided into sections to match discovery methods

os=`uname -s`
if [ "$os" != "Linux" ]; then
    echo This script must be run on Linux
    exit 1
fi

# Set PATH
PATH=/bin:/usr/bin:/sbin:/usr/sbin
export PATH

# Initialisation

tw_locale=`locale -a | grep -i en_us | grep -i "utf.*8" | head -n 1 2>/dev/null`

LANGUAGE=""
if [ "$tw_locale" != "" ]; then
    LANG=$tw_locale
    LC_ALL=$tw_locale
else
    LANG=C
    LC_ALL=C
fi
export LANG LC_ALL


# Stop alias commands changing behaviour.
unalias -a

# Insulate against systems with -u set by default.
set +u

if [ -w /tmp ] 
then
    # use a /tmp file to capture stderr
    TW_CAPTURE_FILE=/tmp/tideway_status_$$
    export TW_CAPTURE_FILE
    rm -f $TW_CAPTURE_FILE

    tw_capture(){
        TW_NAME=$1
        shift
        echo begin cmd_status_err_$TW_NAME >>$TW_CAPTURE_FILE
        "$@" 2>>$TW_CAPTURE_FILE
        RETURN_VAL=$?
        echo end cmd_status_err_$TW_NAME >>$TW_CAPTURE_FILE

        echo cmd_status_$TW_NAME=$RETURN_VAL >>$TW_CAPTURE_FILE
        return $RETURN_VAL
    }

    tw_report(){
        if [ -f $TW_CAPTURE_FILE ]
        then 
            cat $TW_CAPTURE_FILE 2>/dev/null
            rm -f $TW_CAPTURE_FILE 2>/dev/null
        fi
    }
else
    # can't write to /tmp - do not capture anything
    tw_capture(){
        shift
        "$@" 2>/dev/null
    }

    tw_report(){
        echo "cmd_status_err_status_unavailable=Unable to write to /tmp"
    }
fi 

# replace the following PRIV_XXX functions with one that has the path to a
# program to run the commands as super user, e.g. sudo. For example
# PRIV_LSOF() {
#   /usr/bin/sudo "$@"
# }

# lsof requires superuser privileges to display information on processes
# other than those running as the current user
PRIV_LSOF() {
  "$@"
}

# This function supports running privileged commands from patterns
PRIV_RUNCMD() {
  "$@"
}

# This function supports privileged cat of files.
# Used in patterns and to get file content.
PRIV_CAT() {
  cat "$@"
}

# This function supports privilege testing of attributes of files.
# Used in conjunction with PRIV_CAT and PRIV_LS
PRIV_TEST() {
  test "$@"
}

# This function supports privilege listing of files and directories
# Used in conjunction with PRIV_TEST 
PRIV_LS() {
  ls "$@"
}

# This function supports privilege listing of file systems and related
# size and usage.
PRIV_DF() {
  "$@"
}

# dmidecode requires superuser privileges to read data from the system BIOS
PRIV_DMIDECODE() {
    "$@"
}

# hwinfo requires superuser privileges to read data from the system BIOS
PRIV_HWINFO() {
    "$@"
}

# mii-tool requires superuser privileges to display any interface speed
# and negotiation settings
PRIV_MIITOOL() {
    "$@"
}

# ethtool requires superuser privileges to display any interface speed
# and negotiation settings
PRIV_ETHTOOL() {
    "$@"
}

# netstat requires superuser privileges to display process identifiers (PIDs)
# for ports opened by processes not running as the current user
PRIV_NETSTAT() {
    "$@"
}

# lputil requires superuser privileges to display any HBA information
PRIV_LPUTIL() {
    "$@"
}

# hbacmd requires superuser privileges to display any HBA information
PRIV_HBACMD() {
    "$@"
}

# Xen's xe command requires superuser privileges
PRIV_XE(){
    "$@"
}

# esxcfg-info command requires superuser privileges
PRIV_ESXCFG(){
    "$@"
}



# Formatting directive
echo FORMAT Linux

# getDeviceInfo
echo --- START device_info

ihn=`hostname 2>/dev/null`
if [ "$ihn" = "localhost" ]; then
    ihn=`hostname 2>/dev/null`
fi
echo 'hostname:' $ihn
echo 'fqdn:' `hostname --fqdn 2>/dev/null`
dns_domain=`hostname -d 2>/dev/null | sed -e 's/(none)//'` 
if [ "$dns_domain" = "" -a -r /etc/resolv.conf ]; then 
  dns_domain=`awk '/^(domain|search)/ {sub(/\\\\000$/, "", $2); print $2; exit }' /etc/resolv.conf 2>/dev/null` 
fi 
echo 'dns_domain: ' $dns_domain

nis_domain=`domainname 2>/dev/null`
if [ "$nis_domain" == "" ]; then
  nis_domain=`hostname -y 2>/dev/null`
fi
echo 'domain: ' $nis_domain | sed -e 's/(none)//'

os=""
# SuSE lsb_release does not provide service pack so prefer SuSE-release file. 
if [ "$os" = "" -a -r /etc/SuSE-release ]; then
    os=`cat /etc/SuSE-release`
fi
if [ "$os" = "" -a -x /usr/bin/lsb_release ]; then
    # We'd like to use -ds but that puts quotes in the output!
    os=`/usr/bin/lsb_release -d | cut -f2 -d: | sed -e 's/^[ \t]//'`
    if [ "$os" = "(none)" ]; then
        os=""
    else
        # Check to see if its a variant of Red Hat
        rpm -q oracle-logos > /dev/null 2>&1
        if [ $? -eq 0 ]; then
            # Oracle variant
            os="Oracle $os"
        fi
    fi
fi
if [ "$os" = "" -a -r /proc/vmware/version ]; then
    os=`grep -m1 ESX /proc/vmware/version`
fi
if [ "$os" = "" -a -r /etc/vmware-release ]; then
    os=`grep ESX /etc/vmware-release`
fi
if [ "$os" = "" -a -r /etc/redhat-release ]; then
    os=`cat /etc/redhat-release`

    # Check to see if its a variant of Red Hat
    rpm -q oracle-logos > /dev/null 2>&1
    if [ $? -eq 0 ]; then
        # Oracle variant
        os="Oracle $os"
    fi
fi
if [ "$os" = "" -a -r /etc/debian_version ]; then
    ver=`cat /etc/debian_version`
    os="Debian Linux $ver"
fi
if [ "$os" = "" -a -r /etc/mandrake-release ]; then
    os=`cat /etc/mandrake-release`
fi
if [ "$os" = "" ]; then
    os=`uname -sr 2>/dev/null`
fi
echo 'os:' $os
echo 'os_arch:' `uname -m 2>/dev/null`

echo --- END device_info

# getHostInfo
echo --- START host_info


# First, gather information about the processor.
cpuspeed=`egrep '^(cpu MHz|cpu clock|clock)' /proc/cpuinfo | cut -f2 -d: | sed -e 's/\.[0-9]*//' -e 's/Mhz//i' -e 's/ //g' | head -n 1`

cputype=`egrep '^cpu[^a-z]*:' /proc/cpuinfo | sort -u | cut -f2 -d: | sed -e 's/^ //' |  head -n 1`
if [ "${cputype}" = "" ]; then
    cputype=`egrep '^model name' /proc/cpuinfo | sort -u | cut -f2 -d: | sed -e 's/^ //' |  head -n 1`
fi
if [ "${cputype}" = "" ]; then
    cputype=`egrep '^arch' /proc/cpuinfo | sort -u | cut -f2 -d: | sed -e 's/^ //' |  head -n 1`
fi
if [ "${cputype}" = "" ]; then
    cputype=`egrep '^(cpu model|family|vendor_id|machine|Processor)' /proc/cpuinfo | sort -u | cut -f2 -d: | sed -e 's/^ //' |  head -n 1`
fi

# This value is the number of logical processors available
logical=`egrep '^[pP]rocessor' /proc/cpuinfo | sort -u | wc -l`

physical=0
cores=0
threads_per_core=0

if [ "`echo $cputype | cut -c-2`" = "PA" ]; then
    # check if it is a PA-RISC processor
    cpufamily=`egrep '^(cpu family)' /proc/cpuinfo | sort -u | cut -f2 -d: | sed -e 's/^ //' |  head -n 1`
    if [ "`echo $cpufamily | cut -c-7`" = "PA-RISC" ]; then
        cputype="${cpufamily} ${cputype}"
        model=`egrep '^(model name)' /proc/cpuinfo | sort -u | cut -f2 -d: | sed -e 's/^ //' |  head -n 1`
        
        if [ "${model}" != "" ]; then
            echo 'candidate_model[]:' ${model}
        fi
    fi
fi

if  [ "${cputype}" = "Alpha" ]; then
    # Alpha doesn't have one entry per processor.
    cpumodel=`egrep '^(cpu model)' /proc/cpuinfo | sort -u | cut -f2 -d: | sed -e 's/^ //' |  head -n 1`
    cputype="${cputype} ${cpumodel}"
    cores=1
    threads_per_core=1
    physical=`egrep '^(cpus detected)' /proc/cpuinfo | sort -u | cut -f2 -d: | sed -e 's/^ //' |  head -n 1`
    if [ "$physical" = "" ]; then
        physical=1
    fi
    logical=${physical}
    model=`egrep '^(platform string)' /proc/cpuinfo | sort -u | cut -f2 -d: | sed -e 's/^ //' |  head -n 1`
    serial=`egrep '^(system serial number)' /proc/cpuinfo | sort -u | cut -f2 -d: | sed -e 's/^ //' |  head -n 1`
    if [ "${serial}" != "" ]; then
        echo 'candidate_serial[]:' ${serial}
    fi
    
    if [ "${model}" != "" ]; then
        echo 'candidate_model[]:' ${model}
    fi
    
    cpuspeed=`egrep '^(cycle frequency \[Hz\])' /proc/cpuinfo | cut -f2 -d: | sed -e 's/\.[0-9]*//' -e 's/Mhz//i' -e 's/ //g' | head -n 1`
    if [ "${cpuspeed}" != "" ]; then
        cpuspeed=`expr ${cpuspeed} / 1000000`
    fi

elif [ ${logical} -eq 1 ]; then
    # The common case of one logical processor, no need to do
    # anything more fancyful
    #
    physical=1
    cores=1
    threads_per_core=1

elif [ ${logical} -ge 2 ]; then
    # The "siblings" attribute, when present, if fairly reliable. It
    # indicates how many logical processors are running on each
    # physical. Each of those 'sibling' could be a core or a thread
    # or a combination of both.
    siblings=`egrep '^siblings' /proc/cpuinfo | head -n 1 | awk '{print $3}' | tr -d '[:space:]'`
    if [ "${siblings}" = "" ]; then
        siblings=0
    fi
    
    # Some processors are described as dual or quad core in their description.
    # Use this as a hint to confirm the number of cores reported by the OS
    # or use outright if the OS has no clue.
    isDualCore=`echo ${cputype} | egrep -i 'dual.?(core|cpu)'`
    isQuadCore=`echo ${cputype} | egrep -i 'quad.?(core|cpu)'`
    if [ "${isDualCore}" != "" ]; then
        cores_hint=2
    elif [ "${isQuadCore}" != "" ]; then
        cores_hint=4
    else
        isOpteron=`echo ${cputype} | egrep -i 'amd +opteron *\(tm\) +processor.*[0-9][0-9][0-9]'`
        
        if [ "${isOpteron}" != "" ]; then
            # time to try some common CPU detection.
            opteron_number=`expr match "${cputype}" '.*\(.[0-9][0-9][0-9]\)'`
            if [ ${opteron_number} -lt 1000 ]; then
                # Right now numbers below x60 are 1 core
                # Over x60 are dual core
                if [ `echo ${opteron_number} | cut -c2` -lt 6 ]; then
                    cores_hint=1
                    siblings=1
                else
                    cores_hint=2
                    siblings=2
                fi
            else
                # 4 digit processor code. The second digit is the most significant
                opteron_cores=`echo ${opteron_number} | cut -c2`
                if [ ${opteron_cores} -eq 1 ]; then
                    if [ `echo ${opteron_number} | cut -c1` -eq 4 ]; then
                        if [ `echo ${opteron_number} | cut -c3` -lt 6 ]; then
                            # 41xx where xx is lower than 60 are 4 cores
                            cores_hint=4
                            siblings=4
                        else
                            # 41xx where xx is higher than 60 are 6 cores                    
                            cores_hint=6
                            siblings=6
                        fi
                    elif [ `echo ${opteron_number} | cut -c1` -eq 6 ]; then
                        if [ `echo ${opteron_number} | cut -c3` -lt 6 ]; then
                            # 61xx where xx is lower than 60 are 8 cores
                            cores_hint=8
                            siblings=8
                        else
                            # 61xx where xx is higher than 60 are 12 cores                    
                            cores_hint=12
                            siblings=12
                        fi
                    fi
                elif [ ${opteron_cores} -eq 2 ]; then
                    # x2xx are all dual-core
                    cores_hint=2
                    siblings=2
                elif [ ${opteron_cores} -eq 3 ]; then
                    # x3xx are all quad core
                    cores_hint=4
                    siblings=4
                elif [ ${opteron_cores} -eq 4 ]; then
                    # x4xx are 6 cores.
                    cores_hint=6
                    siblings=6
                fi
            fi
        else
            cores_hint=0
        fi
    fi

    # Use "cpu cores" if available. Note that "core id" is not exploitable
    # as the system sometimes assigns a core id to each core on the system
    # and sometimes reuse the same id on two cores on separate processors.
    cores=`egrep '^cpu cores' /proc/cpuinfo | head -n 1 | awk '{print $4}' | tr -d '[:space:]'`
    if [ "${cores}" = "" ]; then
        cores=${cores_hint}
    elif [ ${cores_hint} -ne 0 ]; then
        if [ ${cores_hint} -ne ${cores} ]; then
            cores=0
        fi
    fi
    
    # Do some fixing.
    if [ ${cores} -gt ${siblings} ]; then
        # Taking a risk here, but multicore hyperthreaded cpus are
        # not that common 
        siblings=${cores}
    fi
    
    # If we do not have access to sibling it is not possible to figure out
    # the number of physical processors from the OS

    if [ ${cores} -eq ${logical} ]; then
        # A special case, we got the number of cores, but not the siblings
        # In that case if the number of cores is the same as the number of
        # logical CPU it is safe to assume one physical package.
        physical=1
        threads_per_core=1
        
    elif [ ${siblings} -ne 0 ]; then
        # Check if the number of physical CPUs can be determined. Unfortunately
        # old version of linux sometimes use the same physical ID for separate
        # processors. So this value is retrieved only if we can confirm it via the 
        # siblings
        physical=`egrep '^(physical id)|(Physical processor ID)' /proc/cpuinfo | cut -f2 -d: | sed -e 's/^ //' | sort -u | wc -l`
    
        calculated=`expr ${logical} / ${siblings}`
        
        if [ ${calculated} -ne ${physical} ]; then
            # conflicting information. Better not to take a position rather
            # than being wrong
            if [ ${cores} -eq 0 ]; then
                physical=0
            else
                physical=${calculated}
            fi
        fi
        
        # There is no easy way to find out the number of threads running in each
        # processor. Relying on the htt flag is not an option as this flag is
        # set on processors on which we know there is not hyperthreading at all.
        # So this value is set only if we get good data on cores and siblings.
        if [ ${cores} -ne 0 ]; then
            threads_per_core=`expr ${siblings} / ${cores}`
        fi

    fi
    
fi

# Please do not remove the next line.
# It is used as an anchor for the test suite.
print=1


logical_ram=`awk '/^MemTotal:/ {print $2 "KB"}' /proc/meminfo 2>/dev/null`

if [ -f /usr/sbin/esxcfg-info ]; then
    # On a VMWare ESX controller, report the *real* hardware information
    file=/tmp/tideway-hw-$$
    uuid=""
    PRIV_ESXCFG /usr/sbin/esxcfg-info --hardware > ${file} 2>/dev/null
    if [ $? -eq 0 ]; then
        physical=`grep "Num Packages\." ${file} | sed -e "s/[^0-9]//g"`
        logical=`grep "Num Cores." ${file} | head -n 1 | sed -e "s/[^0-9]//g"`
        cores=`expr ${logical} / ${physical}`
        total_threads=`grep "Num Threads\." ${file} | sed -e "s/[^0-9]//g"`
        threads_per_core=`expr ${total_threads} / ${logical}`
        cpuspeed=`egrep -i "cpu ?speed\." ${file} | head -n 1 | sed 's/[^0-9]*//g' | awk '{printf( " @ %.1f GHz\n", $1/1024**3)}'`
        tmp=`echo ${cputype} | sed 's/ @.*$//'`
        cputype="${tmp} ${cpuspeed}"
        ram=`grep "Physical Mem\." ${file} | sed 's/[^0-9]*//g'`B
        # For esx/esxi, we should NOT use memory from dmesg or /proc/meminfo 
        # because the values are incorrect
        logical_ram=""
        uuid=`grep "BIOS UUID\." ${file}`
    else
        print=0
    fi
    rm -f ${file}

    # Get UUID as hostid if possible
    if [ "$uuid" != "" ]; then
        # Process horrid BIOS UUID format :(
        echo "$uuid" | sed -e 's/\./ /g' | awk '{
printf("hostid: ");
for(i = 3 ; i < 19; i++)
{
    printf("%02s", substr($i,3,2));
    if (i == 6 || i == 8 || i == 10 || i == 12) printf("-");
}
printf("\n");
}'
    else
        if [ -r /etc/slp.reg ]; then
            uuid=`grep hardwareUuid /etc/slp.reg | cut -f2 -d= | tr '[:upper:]' '[:lower:]' | sed -e 's/"//g'`
            if [ "${uuid}" != "" ]; then
                echo 'hostid:' ${uuid}
            fi
        fi
    fi
fi
if [ -f /opt/xensource/bin/xe ]; then
    print=0
    # /proc/cpuinfo reports incorrectly for Xen domains, use "xe"
    # However, this can only tell us the logical processor count, not
    # physical, core count or threads per core
    XE=/opt/xensource/bin/xe
    cores=0
    threads_per_core=0
    physical=0
    cpu_list=`PRIV_XE $XE host-cpu-list 2>/dev/null`
    if [ $? -eq 0 ]; then

        logical=`echo "$cpu_list" | grep uuid | wc -l`
        uuid=`echo "$cpu_list" | grep uuid | head -n 1 | cut -f2 -d: | awk '{print $1;}'`
        cputype=`PRIV_XE $XE host-cpu-param-get uuid=$uuid param-name=modelname`
        cpuspeed=`PRIV_XE $XE host-cpu-param-get uuid=$uuid param-name=speed`

        # /proc/meminfo reports incorrectly for Xen domains, use "xe"
        uuid=`PRIV_XE $XE host-list | grep uuid | head -n 1 | cut -f2 -d: | awk '{print $1;}'`
        logical_ram=`PRIV_XE $XE host-param-get uuid=$uuid param-name=memory-total`
        print=1
    fi
fi

echo 'kernel:' `uname -r`

if [ ${print} -eq 1 ]; then
    # Report processor/memory info
    if [ ${logical} -ne 0 ]; then
        echo 'num_logical_processors:' ${logical}
    fi
    if [ ${cores} -ne 0 ]; then
        echo 'cores_per_processor:' ${cores}
    fi
    if [ ${threads_per_core} -ne 0 ]; then
        echo 'threads_per_core:' ${threads_per_core}
    fi
    if [ ${physical} -ne 0 ]; then
        echo 'num_processors:' ${physical}
    fi
    if [ "${cputype}" != "" ]; then
        echo 'processor_type:' ${cputype}
    fi
    if [ "${cpuspeed}" != "" ]; then
        echo 'processor_speed:' ${cpuspeed}
    fi
    if [ "${ram}" != "" ]; then
        echo 'ram:' ${ram}
    fi
    if [ "${logical_ram}" != "" ]; then
        echo 'logical_ram:' ${logical_ram}
    fi
fi

# Get uptime in days and seconds
uptime | awk '
{ 
  if ( $4 ~ /day/ ) { 
    print "uptime:", $3; 
    z = split($5,t,":"); 
    printf( "uptimeSeconds: %d\n", ($3 * 86400) + (t[1] * 3600) + (t[2] * 60) ); 
  } else { 
    print "uptime: 0"; 
    z = split($3,t,":"); 
    print "uptimeSeconds:", (t[1] * 3600) + (t[2] * 60); 
  }
}'

# zLinux?
if [ -r /proc/sysinfo -a -d /proc/dasd ]; then
    echo "candidate_vendor[]:" `egrep '^Manufacturer:' /proc/sysinfo | awk '{print $2;}'`
    type=`egrep '^Type:' /proc/sysinfo | awk '{print $2;}'`
    model=`egrep '^Model:' /proc/sysinfo | awk '{print $2;}'`
    echo "candidate_model[]: $type-$model"
    echo "zlinux_sequence:" `egrep '^Sequence Code:' /proc/sysinfo | awk '{print $3;}'`
    echo "zlinux_vm_name:" `egrep '^VM00 Name:' /proc/sysinfo | awk '{print $3;}'`
    echo "zlinux_vm_software:" `egrep '^VM00 Control Program:' /proc/sysinfo | awk '{print $4, $5;}'`
fi

# Can we get information from the BIOS? We use lshal if available as that
# requires no superuser permissions but we attempt to run all tools as some
# can return invalid values in some cases. The system will select the "best"
# candidate from the values returned, where "best" is the first non-bogus value
if [ -x /usr/bin/lshal ]; then 
    /usr/bin/lshal 2>/dev/null | sed -e 's/(string)$//g' -e "s/'//g" | awk '
    $1 ~ /(smbios\.system|system\.hardware)\.serial/ {
        sub(/.*(smbios\.system|system\.hardware).serial = */, "");
        printf("candidate_serial[]: %s\n", $0);
    }
    $1 ~ /(smbios\.system|system\.hardware)\.uuid/ {
        sub(/.*(smbios\.system|system\.hardware)\.uuid = */, "");
        printf("candidate_uuid[]: %s\n", $0);
    }
    $1 ~ /(smbios\.system|system\.hardware)\.product/ {
        sub(/.*(smbios\.system|system\.hardware)\.product = */, "");
        printf("candidate_model[]: %s\n", $0);
    }
    $1 ~ /system(\.hardware)?\.vendor/ {
        sub(/.*(system|system\.hardware)\.vendor = */, "");
        printf("candidate_vendor[]: %s\n", $0);
    }'
fi
if [ -f /usr/sbin/dmidecode ]; then
        PRIV_DMIDECODE /usr/sbin/dmidecode 2>/dev/null | sed -n '/DMI type 1,/,/^Handle 0x0/p' | awk '
    $1 ~ /Manufacturer:/ { sub(".*Manufacturer: *", ""); printf("candidate_vendor[]: %s\n", $0); }
    $1 ~ /Vendor:/ { sub(".*Vendor: *", ""); printf("candidate_vendor[]: %s\n", $0); }
    $1 ~ /Product/ && $2 ~ /Name:/ { sub(".*Product Name: *", ""); printf("candidate_model[]: %s\n", $0); }
    $1 ~ /Product:/ { sub(".*Product: *",""); printf("candidate_model[]: %s\n", $0 ); }
    $1 ~ /Serial/ && $2 ~ /Number:/ { sub(".*Serial Number: *", ""); printf("candidate_serial[]: %s\n", $0); }
    $1 ~ /UUID:/ { sub(".*UUID: *", ""); printf( "candidate_uuid[]: %s\n", $0 ); } '
fi
if [ -f /usr/sbin/hwinfo ]; then
        PRIV_HWINFO /usr/sbin/hwinfo --bios 2>/dev/null | sed -n '/System Info:/,/Info:/p' | awk '
    $1 ~ /Manufacturer:/ { sub(".*Manufacturer: *", ""); gsub("\"", ""); printf("candidate_vendor[]: %s\n", $0); }
    $1 ~ /Product:/ { sub(".*Product: *", ""); gsub("\"", ""); printf("candidate_model[]: %s\n", $0); }
    $1 ~ /Serial:/ { sub(".*Serial: *", ""); gsub("\"", ""); printf("candidate_serial[]: %s\n", $0); }
    $1 ~ /UUID:/ { sub(".*UUID: *", ""); gsub("\"", ""); printf("candidate_uuid[]: %s\n", $0); } ' 
fi
if [ -d /sys/class/dmi/id ]; then
    echo "candidate_model[]: " `cat /sys/class/dmi/id/product_name 2>/dev/null`
    echo "candidate_serial[]: " `PRIV_CAT /sys/class/dmi/id/product_serial 2>/dev/null`
    echo "candidate_uuid[]: " `PRIV_CAT /sys/class/dmi/id/product_uuid 2>/dev/null`
    echo "candidate_vendor[]: " `cat /sys/class/dmi/id/sys_vendor 2>/dev/null`
fi

# PPC64 LPAR?
if [ -r /proc/ppc64/lparcfg ]; then
    echo begin lparcfg:
    cat /proc/ppc64/lparcfg 2>/dev/null
    echo end lparcfg
fi
# LPAR name?
if [ -r /proc/device-tree/ibm,partition-name ]; then
    echo "lpar_partition_name:" `cat /proc/device-tree/ibm,partition-name`
fi

echo --- END host_info

# getMACAddresses
echo --- START ip_link_mac
ip -o link show 2>/dev/null
echo --- END ip_link_mac

echo --- START ifconfig_mac
ifconfig -a 2>/dev/null
echo --- END ifconfig_mac

# getIPAddresses
echo --- START ip_addr_ip
ip address show 2>/dev/null
echo --- END ip_addr_ip

echo --- START ifconfig_ip
ifconfig -a 2>/dev/null
echo --- END ifconfig_ip

# getNetworkInterfaces
echo --- START ip_link_if
ip -o link show 2>/dev/null
if [ $? -eq 0 ]; then

ETHTOOL=""
if [ -f /sbin/ethtool ]; then
    ETHTOOL=/sbin/ethtool
else
    if [ -f /usr/sbin/ethtool ]; then
        ETHTOOL=/usr/sbin/ethtool
    fi
fi

MIITOOL=""
if [ -f /sbin/mii-tool ]; then
    MIITOOL=/sbin/mii-tool
fi

echo 'begin details:'
for i in `ip -o link show 2>/dev/null | egrep '^[0-9]+:' | awk -F: '{print $2;}'`
do
    if [ -d /sys/class/net/$i ]; then
        echo begin /sys/class/net/$i:
        echo name: $i
        for file in address duplex ifindex speed
        do
            path=/sys/class/net/$i/$file
            if [ -r $path ]; then
                value=`cat $path 2>/dev/null`
                if [ "$value" != "" ]; then
                    echo $file: $value
                fi
            fi
        done

        if [ -d /sys/class/net/$i/bonding ]; then
            # Interface is a bonding master
            echo bonded: True
            for file in mode slaves
            do
                path=/sys/class/net/$i/bonding/$file
                if [ -r $path ]; then
                    value=`cat $path 2>/dev/null`
                    if [ "$value" != "" ]; then
                        echo bonding_$file: $value
                    fi
                fi
            done
        fi
        echo end /sys/class/net/$i:
    fi

    SUCCESS=1
    if [ "$ETHTOOL" != "" ]; then
        echo begin ethtool-$i:
        PRIV_ETHTOOL $ETHTOOL $i 2>/dev/null
        SUCCESS=$?
        echo end ethtool-$i:
    fi
    if [ "$MIITOOL" != "" -a $SUCCESS -ne 0 ]; then
        echo begin mii-tool-$i:
        PRIV_MIITOOL $MIITOOL -v $i 2>/dev/null
        echo end mii-tool-$i:
    fi
done
echo 'end details:'
fi

echo --- END ip_link_if

echo --- START ifconfig_if
ifconfig -a 2>/dev/null
if [ $? -eq 0 ]; then

ETHTOOL=""
if [ -f /sbin/ethtool ]; then
    ETHTOOL=/sbin/ethtool
else
    if [ -f /usr/sbin/ethtool ]; then
        ETHTOOL=/usr/sbin/ethtool
    fi
fi

MIITOOL=""
if [ -f /sbin/mii-tool ]; then
    MIITOOL=/sbin/mii-tool
fi

echo 'begin details:'
for i in `ifconfig -a 2>/dev/null | egrep '^[a-z]' | awk -F: '{print $1;}'`
do
    if [ -d /sys/class/net/$i ]; then
        echo begin /sys/class/net/$i:
        echo name: $i
        for file in address duplex ifindex speed
        do
            path=/sys/class/net/$i/$file
            if [ -r $path ]; then
                value=`cat $path 2>/dev/null`
                if [ "$value" != "" ]; then
                    echo $file: $value
                fi
            fi
        done

        if [ -d /sys/class/net/$i/bonding ]; then
            # Interface is a bonding master
            echo bonded: True
            for file in mode slaves
            do
                path=/sys/class/net/$i/bonding/$file
                if [ -r $path ]; then
                    value=`cat $path 2>/dev/null`
                    if [ "$value" != "" ]; then
                        echo bonding_$file: $value
                    fi
                fi
            done
        fi
        echo end /sys/class/net/$i:
    fi

    SUCCESS=1
    if [ "$ETHTOOL" != "" ]; then
        echo begin ethtool-$i:
        PRIV_ETHTOOL $ETHTOOL $i 2>/dev/null
        SUCCESS=$?
        echo end ethtool-$i:
    fi
    if [ "$MIITOOL" != "" -a $SUCCESS -ne 0 ]; then
        echo begin mii-tool-$i:
        PRIV_MIITOOL $MIITOOL -v $i 2>/dev/null
        echo end mii-tool-$i:
    fi
done
echo 'end details:'
fi

echo --- END ifconfig_if

# getNetworkConnectionList
echo --- START netstat

PRIV_NETSTAT netstat -aneep --tcp --udp -T 2>/dev/null
if [ $? -eq 4 ]; then
    # netstat failed due to invalid option, try -W
    PRIV_NETSTAT netstat -aneep --tcp --udp -W 2>/dev/null
    if [ $? -eq 4 ]; then
        # netstat still failed, try without any wide option
        PRIV_NETSTAT netstat -aneep --tcp --udp 2>/dev/null
    fi
fi

echo --- END netstat

# getProcessList
echo --- START ps
ps -eo pid,ppid,uid,user,cmd --no-headers -ww 2>/dev/null

echo --- END ps

# getPatchList
#   ** DISABLED **

# getProcessToConnectionMapping
echo --- START lsof-i
PRIV_LSOF lsof -l -n -P -F ptPTn -i 2>/dev/null
echo --- END lsof-i

# getPackageList
echo --- START rpmx
rpm -qa --queryformat 'begin\nname: %{NAME}\nversion: %{VERSION}\nrelease: %{RELEASE}\narch: %{ARCH}\nvendor: %{VENDOR}\nepoch: %{EPOCH}\ndescription: %{SUMMARY}\nend\n' 2>/dev/null

echo --- END rpmx

echo --- START rpm
rpm -qa --queryformat %{NAME}:%{VERSION}:%{RELEASE}:%{ARCH}:%{EPOCH}@ 2>/dev/null

echo
echo --- END rpm

echo --- START dpkg
COLUMNS=256 dpkg -l '*' 2>/dev/null | egrep '^ii '

echo --- END dpkg

# getHBAList
echo --- START hba_sysfs
echo begin sysfs_hba:
if [ ! -d /sys/class ]; then
    echo /sys/class does not exist
elif [ -r /proc/vmware/version ] && 
     [ `grep -o -m1 ESX /proc/vmware/version` ] ||
     [ -r /etc/vmware-release ] && 
     [ `grep -o ESX /etc/vmware-release` ]; then
    echo ESX HBA drivers do not support sysfs
else
    for device in `ls /sys/class/fc_host 2>/dev/null`
    do
        systool -c fc_host -v $device
        systool -c scsi_host -v $device
    done
fi
echo end sysfs_hba:

echo --- END hba_sysfs

echo --- START hba_procfs
echo begin procfs_hba:
if [ ! -d /proc/scsi ]; then
    echo /proc/scsi does not exist
else
    for driver in 'qla*' 'lpfc*'
    do
        for device in `find /proc/scsi/$driver/* 2>/dev/null`
        do
            echo HBA Port $device
            cat $device
        done
    done
fi
echo end procfs_hba:

echo --- END hba_procfs

echo --- START hba_hbacmd

PATH=/usr/sbin/hbanyware:$PATH

echo begin hbacmd_listhbas:
PRIV_HBACMD hbacmd ListHBAs
echo end hbacmd_listhbas:

echo begin hbacmd_hbaattr:
for WWPN in `PRIV_HBACMD hbacmd ListHBAs 2>/dev/null | awk '/Port WWN/ {print $4;}'`
do
    PRIV_HBACMD hbacmd HBAAttrib $WWPN 2>/dev/null
done
echo end hbacmd_hbaattr:

echo --- END hba_hbacmd

# getServices
#   ** UNSUPPORTED **

# getFileSystems
echo --- START df
echo begin df:
PRIV_DF df -lk 2>/dev/null
echo end df:
echo begin mount:
mount 2>/dev/null
echo end mount:
echo begin xtab:
if [ -s /var/lib/nfs/xtab ]; then
    cat /var/lib/nfs/xtab
else
    if [ -s /var/lib/nfs/etab ]; then
        cat /var/lib/nfs/etab
    fi
fi
echo end xtab:
echo begin smbclient:
smbclient -N -L localhost
echo end smbclient:
echo begin smbconf:
configfile=`smbstatus -v 2>/dev/null | grep "using configfile" | awk '{print $4;}'`
if [ "$configfile" != "" ]; then
    if [ -r $configfile ]; then
        cat $configfile
    fi
fi
echo end smbconf:

echo --- END df


#!/bin/bash
    date;
    echo "uptime:"
    uptime
    echo "Currently connected:"
    w
    echo "--------------------"
    echo "Last logins:"
    last -a |head -3
    echo "--------------------"
    echo "Disk and memory usage:"
    df -h | xargs | awk '{print "Free/total disk: " $11 " / " $9}'
    free -m | xargs | awk '{print "Free/total memory: " $17 " / " $8 " MB"}'
    echo "--------------------"
    start_log=`head -1 /var/log/messages |cut -c 1-12`
    oom=`grep -ci kill /var/log/messages`
    echo -n "OOM errors since $start_log :" $oom
    echo ""
    echo "--------------------"
    echo "Utilization and most expensive processes:"
    top -b |head -3
    echo
	top -b |head -10 |tail -4
    echo "--------------------"
    echo "Open TCP ports:"
    nmap -p- -T4 127.0.0.1
    echo "--------------------"
    echo "Current connections:"
    ss -s
    echo "--------------------"
    echo "processes:"
    ps auxf --width=200
    echo "--------------------"
    echo "vmstat:"
    vmstat 1 5




Script:-
cat list|while read line1 line2
do expect -c 'spawn scp script.sh tatatas@'$line1':/home/tatatas;sleep 1;expect password;send "Passw0rd@123\n";sleep 1;'
expect -c 'spawn ssh tatatas@'$line1';expect password;send "Passw0rd@123\n";expect "*$";send "./script.sh>`hostname`_df.out\n";expect "*$";send exit\n;'
expect -c 'spawn scp -r tatatas@'$line1':/home/tatatas/*.out /tmp/;sleep 1;expect password;send "Passw0rd@123\n";sleep 1;'
done


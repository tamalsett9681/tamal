lsdev –Cc processor
lsattr –EL proc0
bindprocessor
sar –P ALL or
topas, nmon
lparstat
vmstat (use –
iostat
mpstat –s
lssrad -av


vmo -p -o minperm%=3
vmo -p -o maxperm%=90
vmo -p -o maxclient%=90
vmo -p -o minfree=960 We will calculate these
vmo -p -o maxfree=1088 We will calculate these
vmo -p -o lru_file_repage=0
vmo -p -o lru_poll_interval=10
vmo -p -o page_steal_method=1
no -p -o rfc1323=1
no -p -o tcp_sendspace=262144
no -p -o tcp_recvspace=262144
no -p -o udp_sendspace=65536
no -p -o udp_recvspace=655360

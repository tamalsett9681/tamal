DEVICE=eth0 
ONBOOT=yes 
BOOTPROTO=dhcp 
TYPE=Ethernet 
USERCTL=no 
PEERDNS=no 
IPV6INIT=no 
NM_CONTROLLED=yes 
DHCP_HOSTNAME=WENCSVMASPRD01 
ZONE=public 
DNS1=10.48.22.11 
DNS2=10.48.22.12 
DNS3=10.59.22.11 
DOMAIN=prd1.prdroot.net 


DNS 

For West Europe, 
DNS1=10.90.1.11 
DNS2=10.48.22.11 
DNS3=10.59.22.11 
  
For North Europe, 
DNS1=10.80.1.11 
DNS2=10.48.22.11 
DNS3=10.59.22.11 


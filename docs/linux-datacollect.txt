touch /home/capture.txt
echo "#####################################################################" >> /home/capture.txt
echo "# uname -a" >> /home/capture.txt

uname -a >> /home/capture.txt
echo "#####################################################################" >> /home/capture.txt
echo "# cat /etc/redhat-release" >> /home/capture.txt
cat /etc/redhat-release >> /home/capture.txt

echo "#####################################################################" >> /home/capture.txt
echo "# ifconfig -a" >> /home/capture.txt

ifconfig -a >> /home/capture.txt
echo "#####################################################################" >> /home/capture.txt

echo "# netstat -rn" >> /home/capture.txt

netstat -rn >> /home/capture.txt
echo "#####################################################################" >> /home/capture.txt

echo "# df -kh" >> /home/capture.txt

df -kh >> /home/capture.txt
echo "#####################################################################" >> /home/capture.txt
echo "# pvdisplay -v" >> /home/capture.txt

pvdisplay -v >> /home/capture.txt

echo "#####################################################################" >> /home/capture.txt
echo "# cat /etc/sysconfig/network-scripts.ifcfg-eth0" >> /home/capture.txt

cat /etc/sysconfig/network-scripts/ifcfg-eth0 >> >> /home/capture.txt

echo "#####################################################################" >> /home/capture.txt
echo "# cat /etc/sysconfig/network-scripts.ifcfg-eth1" >> /home/capture.txt

cat /etc/sysconfig/network-scripts/ifcfg-eth1 >> >> /home/capture.txt

echo "#####################################################################" >> /home/capture.txt
echo "# cat /etc/sysconfig/network-scripts.ifcfg-eth3" >> /home/capture.txt

cat /etc/sysconfig/network-scripts/ifcfg-eth3 >> >> /home/capture.txt


echo "#####################################################################" >> /home/capture.txt
echo "# lvdisplay -av" >> /home/capture.txt

lvdisplay -av >> /home/capture.txt
echo "#####################################################################" >> /home/capture.txt
echo "# vgdisplay -v" >> /home/capture.txt

vgdisplay -v >> /home/capture.txt
echo "#####################################################################" >> /home/capture.txt
echo "# cat /etc/grub.conf" >> /home/capture.txt
cat /etc/grub.conf >> /home/capture.txt

echo "#####################################################################" >> /home/capture.txt
echo "# rpm -qa " >> /home/capture.txt
rpm -qa >> /home/capture.txt
echo "#####################################################################" >> /home/capture.txt


yum check-update > /home/`hostname`_patchinfo_`date '+%m-%d-%Y_%H%M'`.txt


Push and pull modes
The push mode operation is initiated from the master. The pull mode operation is
initiated from the client. The very first time a client is installed, only the pull mode
can be used. Note the following points:
==> In order for the push mode to be successful, the client must have a minimum
AIX image already installed and TCP/IP configured.
==> To use the pull mode, you need access to the clients’ SMS menu. For this you
either need a console attached to the machine, or for HMC-managed
systems, you need access to the HMC.


lsnim -c machines    -----    List of all nim objects having class Machines

lsnim -t spot      -------    list the different available SPOT resources.

lsnim -l <nim-client>    -----     To check the details of a NIM Client
Where "Cstate" field determines whether the machine object is in use.

lsnim -l -c networks

lsnim -l <nim-network>    ------    Tocheck the details of a NIM Network.
The Nstate field determines whether the network object is in use. If the Nstate value displayed is different from ready for a NIM operation, you cannot perform any operation on this object.

lsnim -l <resources>    ------    Tocheck the details of a NIM Resource.
Another important attribute is Rstate (for “resource state”). The status shown determines whether the object is currently in use or not. 
If the Rstate is not ready, it cannot be used and a warning message will appear on the console.

lsnim -O lpp5300  ----  To display operations that can be performed on a NIM lpp_source

nim -Fo reset spot5300  -----  Resetting a NIM SPOT

nim -o check spot5300  -----  To check operation to verify the usability of the SPOT in the NIM environment.

nim -o lppchk -a show_progress=yes spot5300    -----   Checking the contents of a SPOT

Groups class
A group is a collection of either machines or resources. Only two types are available:
1. res_group, for grouping a set of resources
2. mac_group, for grouping a set of machines

nim -o define -t mac_group -a add_member=uataix01nim -a add_member=uataix02nim -a add_member=uataix03nim -a add_member=uataix04nim -a add_member=uataix05nim
-a add_member=uataix06nim nimgrp1     -------------   Creating a machine group

nim -o allocate -a group=basic_res_grp dkdc1clsinfa001   ----   Allocating Resource Group to a Server

lsnim -l <group>   --------   To check the members of a Group

To start a machine installation, only two resources are mandatory: the LPP source (lpp_source), and the Shared Product Object Tree (SPOT).

SPOT
SPOT, or Shared Product Object Tree, is a directory of code (installed filesets) that is used during client booting procedure. It is equivalent in content
to the code that resides in the /usr file system on a system running AIX 5L. This resource (directory) replaces the content of the basic ramdisk image available on installation CDs.
Device drivers, the BOS install program, and other necessary code needed to perform a base operating system installation are found inside the SPOT.
The SPOT also contains the code needed to generate the boot images (kernels, which will be stored in the /tftpboot directory) that the client uses until it can manage to NFS-mount the SPOT directory.

nim -Fo check <spot-name>   ----   Checking SPOT image

nim -o lslpp AIX_6100-06_SPOT   ----   Checking the packages of SPOT Image

nim -o lppchk -a show_progress=yes dkdc1tstinfa001_spot1    ---    Checking the contents of the spot

nim -o fix_query spot_5200-08 | grep ML   -------   Checking a SPOT level

nim -o cust -a fixes=update_all -a lpp_source=5305_lpp 5305_spot  ----   Update a spot with an lpp_source

lpp_source
An lpp_source is a directory similar to AIX install CDs. It contains AIX Licensed Program Products (LLPs) in Backup File Format (BFF) format and RPM Package Manager (RPM) filesets that you can install.

nim -o showres lpp5300   ---   Checking the filesets of an lpp_source

nim -Fo check <lpp_source>  ------   Checks and rebuilds the .toc file, and determines if all files are included for simages=yes

nim -o lppmgr <lpp_source>   ------    Removes duplicate filesets from an lpp_source

nim -o lppmgr -a lppmgr_flags=rub <lpp_source>  ------- Removes (-r) all duplicate updates (-u) and duplicate base levels (-b)

nim -o update -a source=/dev/cd0 -a packages=all 5305_lpp --------- Add software to lpp_source

bosinst_data
The bosinst_data resource is a flat ASCII file similar to the bosinst.data file used for restoring system backup images from tape or CD/DVD. 
When you start the installation of a IBM System p machine, you will be prompted to select the console, language, installation method, the target disk(s) for rootvg,
and so on. This resource comes in handy for both push or pull installation of multiple machines at the same time, because it will automate the installation
process without requiring interactive answers for the installation parameters.

/usr/lpp/bosinst/bicheck <bosinst file-name>    -----    Checking the a bosinst_data file

script
The script resource is a file. After the BOS installation is finished on your client, you can perform customization such as file system resizing, additional
user creation, and so forth. The script resource points to a file (usually a shell script) that contains all the commands needed to perform customization.

Important: The NIM master should always have the highest version of AIX installed across the entire NIM environment. Using the same AIX level for
master and client is acceptable.

Packages required for NIM Master:
bos.sysmgt.nim.master
bos.sysmgt.nim.spot

smitty nimconfig  ----  For NIM Master Configuration
smitty nim_mkres  ----  To create NIM resources
smitty nim_mkmac ---- To create NIM Clients
smitty nim_task_inst --- To install Base OS on clients
smitty eznim  ------  NIM Advanced Configuration


smitty nim-> Configure the NIM Environment-> configure a Basic NIM Environment (Easy Startup)    --------   To create the lpp_source and SPOT together, instead of creating individual resources.

We suggest that you create the lpp_source first from CD, then use it to create the SPOT. This way you use the CD only once, thus reducing the time for creating the SPOT.

tail /etc/bootptab

The NIM update operation, which allows you to update an lpp_source resource by adding or removing packages. Previously, you could copy or
remove packages into or from an lpp_source directory and then run the "nim -o check" command to update the lpp_source.

nim_master_setup
The nim_master_setup command installs the bos.sysmgt.nim.master fileset, configures the NIM master, and creates the
resources for installation (SPOT, lpp_source), optionally including a system backup resource (mksysb). The nim_master_setup command uses the rootvg
volume group and creates an /export/nim file system by default. You can change these defaults using the volume_group and file_system options.

nim_master_setup -a volume_group=nimvg    ----    Creating NIM Master in a specified VG.

nim_clients_setup
The nim_clients_setup command as shown in Example 2-33, is used to define your NIM clients, allocate the installation resources, and initiate a NIM
BOS installation on the clients.

niminit -v -a master=dkdc1niminfa001 -a name=dkdc1tstinfa001 -a pif_name=en0 -a platform=chrp -a netboot_kernel=64 -a connect=nimsh/shell    ------------   For client configuration/Generate new /etc/niminfo file

tftpd and bootpd services are required for NIM

nimconfig -a netname=net_10_1_1 -a pif_name=en0 -a netboot_kernel=mp -a cable_type=tp -a client_reg=yes   -------   Creating NIM Master

alog -f /usr/adm/ras/nimlog -o   ----------   Checking NIM Log Files

nim -o remove lpp5300   ---   Removing an lpp_source

/usr/lpp/bos.sysmgt/nim/methods/m_backup_db /tmp/nimdb_backup   -----------    backing up NIM Master Configuration

nim -o unconfig master   ------    Unconfigure a NIM Master

nim -o define -t standalone -a if1="net_10_1_1 lpar55 0 ent0" LPAR55  -------   Creating a standalone NIM Client

nim -o allocate -a spot=spot5304 -a lpp_source=lpp5304 LPAR55  -----  Allocating resources on NIM Client

nim -o bos_inst -a source=rte -a installp_flags=agX -a accept_licenses=yes LPAR55   -----    Initiating BOS Installation on NIM Client

nim -Fo reset LPAR55   ----   Resetting resources on NIM Client

nim -Fo deallocate -a subclass=all LPAR55  ----  Deallocating all resources allcated to NIM Client

nim -o showlog -a log_type=boot LPAR55  ----  To view the progress during installation and first boot

nim -o showlog -a log_type=bosinst LPAR55   ----   To check BOS Installation Console

nim -o define -t res_group -a bosinst_data=5300-04bid_ow -a lpp_source=530lpp_res -a spot=530spot_res -a mksysb=5300-04master_sysb basic_res_grp   ------  Defining Resource Group

lsnim -a Cstate -a Mstate LPAR4  ----    Checking Cstate & Mstate of NIM Client

nim -o define -t spot -a server=master -a location=/export/spot -a source=dkdc1tstinfa001_mksysb -a installp_flags=-aQg spot5300  ----  Creating a SPOT image from mksysb

nim -o define -t mksysb -a server=master -a location=/usr/bkp_mksysb/nimc_mksysb -a source=nimc -a mk_image=yes -a mksysb_flags=i -a comments="mksysb backup of ides server" nimc_mksysb    ----   Creating mksysb resource taking mksysb backup from NIM Client

nim -o alt_disk_install -a source=rootvg -a disk=hdisk1 -a set_bootlist=no dkdc1tstinfa001   ----   Initiating Alt_disk_install in NIM Client from NIM Server

nim -o alt_disk_install -a source=rootvg -a disk=hdisk1 -a fixes=update_all -a lpp_source=ATX7_TL03_SP04 -a set_bootlist=no -a accept_licenses=yes -a installp_flags="-acNgXY" dkdc1tstinfa001   ----   Initiating Alt_disk_install with patching in NIM Client from NIM Master

If there is no mksysb resource available yet, the only way to perform a BOS install on a client is by using a Run-Time Environment (rte).


Examples:
=============
nim -o showlog -a log_type=alt_disk_install dkdc1tstinfa001_infra

nim -o define -t mksysb -a server=master -a location=/mksysb/dkdc1tstinfa001_mksysb dkdc1tstinfa001_mksysb

nim -o define -t spot -a server=master -a location=/export/spot -a source=dkdc1clsinfa001_mksysb -a installp_flags=-aQg dkdc1clsinfa001_spot

nim -o allocate -a spot=dkdc1clsinfa001_spot -a mksysb=dkdc1clsinfa001_mksysb -a lpp_source=aix61_lpp -a bosinst_data=bosinst dkdc1clsinfa001

nim -o bos_inst -a source=mksysb -a installp_flags=agX -a no_nim_client=yes -a accept_licenses=yes -a boot_client=no dkdc1clsinfa001

nim -o allocate -a spot=dkdc1clsinfa001_spot -a mksysb=dkdc1clsinfa001_mksysb dkdc1clsinfa001

nim -o define -t mksysb -a server=master -a location=/export/nim/mksysb/dkdc1clsinfa001_mksysb -a source=dkdc1clsinfa001 -a mk_image=yes -a mksysb_flags=i -a comments="mksysb backup of cluser infra server" dkdc1clsinfa001_mksysb

nim -o define -t lpp_source -a server=master -a location=/export/lpp_source/aix61_lpp -a source=/dev/cd0 aix61_lpp

niminit -v -a master=dkdc1tstinfa001 -a name=dkdc1clsinfa001 -a pif_name=en0 -a platform=chrp -a netboot_kernel=64 -a connect=nimsh

smitty nim_config_services  ---  To change the ports for communication from NIM Client with Master





#!/bin/sh

HOST=`hostname`
LOGFILE=/tmp/post-provisioning-`date '+%d%m%Y'`.log

# Disable Firewall
echo "=======================================" > $LOGFILE
echo "Time: `date '+%d-%m-%Y %H:%M:%S'` Disabling Firewall and Selinux ..." >> $LOGFILE
echo "=======================================" >> $LOGFILE
systemctl stop firewalld >> $LOGFILE 2>&1
systemctl disable firewalld >> $LOGFILE 2>&1
sed -i '7s/.*/SELINUX=disabled/' /etc/sysconfig/selinux
sed -i '7s/.*/SELINUX=disabled/' /etc/selinux/config
setenforce 0 >> $LOGFILE 2>&1

#creating local users, which exists
echo -e "\n\n=======================================" >> $LOGFILE
echo "Time: `date '+%d-%m-%Y %H:%M:%S'` Creating Admin User ..." >> $LOGFILE
echo "=======================================" >> $LOGFILE
groupadd srv_acct >> $LOGFILE 2>&1
useradd -u 1101 -g srv_acct -c "TCS System Admin" -p '$6$r2UWxo/bw4iCMjY5$s5vxwh3P15mvdCyjlZjxpP83OP2RgU5NWg.EVIBAEzIDGhJ.fbBz91lTMzNz0PpvzGwgzuCHbl4pwG29xZBA8.' tcsuser >> $LOGFILE 2>&1
useradd -u 1102 -g srv_acct -c "Backup User" -p '$6$TNmcbsE8I5r3qO.L$4yf5fBUZ9VAsux3gLCjmOJs5lVBAS9eGuwk8vZsiJXi1cxJDQ.w5NLlK3ifgjj8wO26ODcrp/MjwxueXFq42B.' bkpadmin  >> $LOGFILE 2>&1
useradd -u 1103 -c "Device42 User" -p '$6$FoQy.rmwP25JdfTG$Da5CTNQIMfArWc8eOf2VxBG.ZHrkPM07AYOwQA3.D821BuC07.rNO2DOlfOOu82/NaljqH5YAuISfl9PIwwm40' device42  >> $LOGFILE 2>&1
#useradd -u 1104 -c "Solarwinds User" -p '$6$P9E2vC1DU4RFHKOG$sVwmQ6oIgnvyNhlHUeTkojEXzPz4I1NRNGMnDN3ABv6WXIJSrjhADlQiGj6yL9sNv/Cr1o4vz8qPUygYMy2ra1' #sa01.ECPSolarwinds >> $LOGFILE 2>&1
#useradd -u 1105 -c "Ignio User" -p '$6$4/uMWJyX5lZOjLbR$b0Xm766ik/6Rqxpz7Ym69ZcbT5AaFsBfVL7n79kgr5Zv02oDsSoNQTnlttCJ5PeYNwyiov013EjARYL16uN4U.' sa01.ignio >> $LOGFILE #2>&1
#useradd -u 1106 -g srv_acct -c "McAfee Admin" -p '$6$IRpaXu3YJ4w7hL6M$EWGH9mTLcG4HCkZYfxTioVFvPcelTZClpsf.TrVeNw1rrEkpQgCj2dQFFnlv14CodyKOpcazcwDgkTwjglWYX1' #mcafee.admin >> $LOGFILE 2>&1
#chage -d 0 mcafee.admin
usermod -p '$6$uh0BnWDPOEUaMiRH$eEkiTD.VnqFgYyaJi3s8IsmNdD99OJNsIdOZ5LQ/vhIw3Vog.18CD1x1iJ2KqIwRjR4Z1wnD/sTkAVVa3hOnx0' root

#setting admin access
echo -e "\n\n=======================================" >> $LOGFILE
echo "Time: `date '+%d-%m-%Y %H:%M:%S'` Providing sudo access to Admin User ..." >> $LOGFILE
echo "=======================================" >> $LOGFILE
/bin/echo "%srv_acct ALL=(ALL) ALL" >> /etc/sudoers

#setting timezone
echo -e "\n\n=======================================" >> $LOGFILE
echo "Time: `date '+%d-%m-%Y %H:%M:%S'` Setting Timezone ..." >> $LOGFILE
echo "=======================================" >> $LOGFILE
/usr/bin/rm -rf /etc/localtime >> $LOGFILE 2>&1
/usr/bin/ln -s /usr/share/zoneinfo/Europe/Copenhagen /etc/localtime >> $LOGFILE 2>&1

#Installing yum packages
echo -e "\n\n=======================================" >> $LOGFILE
echo "Time: `date '+%d-%m-%Y %H:%M:%S'` Installing required Packages ..." >> $LOGFILE
echo "=======================================" >> $LOGFILE
rm -rf /etc/yum.repos.d/*.repo
/bin/echo "[repo-1]" > /etc/yum.repos.d/repo-1.repo
/bin/echo "name=RHEL7 Repository 1" >> /etc/yum.repos.d/repo-1.repo
/bin/echo "baseurl=http://10.48.75.109/rhel-7-server-rpms" >> /etc/yum.repos.d/repo-1.repo
/bin/echo "gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release" >> /etc/yum.repos.d/repo-1.repo
/bin/echo "enabled=1" >> /etc/yum.repos.d/repo-1.repo
/bin/echo "gpgcheck=1" >> /etc/yum.repos.d/repo-1.repo
/bin/echo "proxy=_none_" >> /etc/yum.repos.d/repo-1.repo
/bin/echo "[repo-2]" > /etc/yum.repos.d/repo-2.repo
/bin/echo "name=RHEL7 Repository 2" >> /etc/yum.repos.d/repo-2.repo
/bin/echo "baseurl=http://10.48.75.114/rhel-7-server-rpms" >> /etc/yum.repos.d/repo-2.repo
/bin/echo "gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release" >> /etc/yum.repos.d/repo-2.repo
/bin/echo "enabled=1" >> /etc/yum.repos.d/repo-2.repo
/bin/echo "gpgcheck=1" >> /etc/yum.repos.d/repo-2.repo
/bin/echo "proxy=_none_" >> /etc/yum.repos.d/repo-2.repo
/bin/yum makecache fast >> $LOGFILE 2>&1
yum install sssd realmd oddjob oddjob-mkhomedir adcli samba-common samba-common-tools krb5-workstation chrony unzip nmap telnet nfs-utils net-tools bind-utils psmisc -y >> $LOGFILE 2>&1

# changing the ethernet configuratoin
echo -e "\n\n=======================================" >> $LOGFILE
echo "Time: `date '+%d-%m-%Y %H:%M:%S'` Configuring Network settings ..." >> $LOGFILE
echo "=======================================" >> $LOGFILE
/bin/sed -i 's/BOOTPROTO=dhcp/BOOTPROTO=none/g' /etc/sysconfig/network-scripts/ifcfg-ens192
/bin/sed -i '/DEFROUTE=yes/d' /etc/sysconfig/network-scripts/ifcfg-ens192
/bin/echo "IPADDR=`hostname -i`" >> /etc/sysconfig/network-scripts/ifcfg-ens192
/bin/echo "NETMASK=`ifconfig ens192|grep netmask|awk '{print $4}'`" >> /etc/sysconfig/network-scripts/ifcfg-ens192
/bin/echo "GATEWAY=`ip r|grep default|awk '{print $3}'`" >> /etc/sysconfig/network-scripts/ifcfg-ens192
/bin/echo "DNS1=10.48.22.21" >> /etc/sysconfig/network-scripts/ifcfg-ens192
/bin/echo "DNS2=10.48.22.22" >> /etc/sysconfig/network-scripts/ifcfg-ens192
/bin/echo "DNS3=10.59.22.21" >> /etc/sysconfig/network-scripts/ifcfg-ens192
/bin/echo "DOMAIN=prd1.prdroot.net" >> /etc/sysconfig/network-scripts/ifcfg-ens192
/usr/bin/systemctl restart network >> $LOGFILE 2>&1

# NTP Configuration
echo -e "\n\n=======================================" >> $LOGFILE
echo "Time: `date '+%d-%m-%Y %H:%M:%S'` Configuring NTP/Chrony ..." >> $LOGFILE
echo "=======================================" >> $LOGFILE
/bin/sed -i '3s/.*/#&/' /etc/chrony.conf
/bin/sed -i '4s/.*/#&/' /etc/chrony.conf
/bin/sed -i '5s/.*/#&/' /etc/chrony.conf
/bin/sed -i '6s/.*/#&/' /etc/chrony.conf
/bin/echo "server 10.48.229.242 iburst" >> /etc/chrony.conf 
/bin/echo "server 10.59.229.242 iburst" >> /etc/chrony.conf
/usr/bin/systemctl enable chronyd >> $LOGFILE 2>&1
/usr/bin/systemctl restart chronyd >> $LOGFILE 2>&1
/bin/chronyc makestep >> $LOGFILE 2>&1

# changing password aging
echo -e "\n\n=======================================" >> $LOGFILE
echo "Time: `date '+%d-%m-%Y %H:%M:%S'` Setting Password Aging ..." >> $LOGFILE
echo "=======================================" >> $LOGFILE
/bin/sed -i '25s/.*/PASS_MAX_DAYS   90/' /etc/login.defs
/bin/sed -i '26s/.*/PASS_MIN_DAYS   7/' /etc/login.defs
/bin/sed -i '27s/.*/PASS_MIN_LEN    8/' /etc/login.defs
/bin/sed -i '28s/.*/PASS_WARN_AGE   28/' /etc/login.defs
/bin/sed -i '29s/.*/LOGIN_RETRIES   5\n/' /etc/login.defs

# setting password complexity
echo -e "\n\n=======================================" >> $LOGFILE
echo "Time: `date '+%d-%m-%Y %H:%M:%S'` Setting Password Complexity ..." >> $LOGFILE
echo "=======================================" >> $LOGFILE
/bin/sed -i '6s/.*/auth            required        pam_wheel.so root_only use_uid debug/' /etc/pam.d/su
/usr/bin/systemctl enable --now oddjobd >> $LOGFILE 2>&1
/sbin/authconfig --passalgo=sha512 --passminlen=9 --passminclass=3 --passmaxrepeat=3 --passmaxclassrepeat=4 --enablerequpper --enablereqlower --enablereqdigit --enablereqother --enablefaillock  --faillockargs="audit deny=5 fail_interval=900 unlock_time=0" --enablemkhomedir --update >> $LOGFILE 2>&1
/bin/echo "difok = 5" >> /etc/security/pwquality.conf
/bin/echo "gecoscheck = 1" >> /etc/security/pwquality.conf
/bin/wget -q -O /etc/pam.d/system-auth-tryg http://10.48.64.61/system-auth-tryg >> $LOGFILE 2>&1
/bin/wget -q -O /etc/pam.d/password-auth-tryg http://10.48.64.61/password-auth-tryg >> $LOGFILE 2>&1
cd /etc/pam.d; unlink system-auth; unlink password-auth; ln -s system-auth-tryg system-auth; ln -s password-auth-tryg password-auth; cd ~
/bin/sed -i '81i\Defaults    logfile = /var/log/sudo.log' /etc/sudoers

# changing ssh configuration
echo -e "\n\n=====================================================" >> $LOGFILE
echo "Time: `date '+%d-%m-%Y %H:%M:%S'` Modifying SSH configuration with command history ..." >> $LOGFILE
echo "=====================================================" >> $LOGFILE
/bin/sed -i '39i\PermitRootLogin no' /etc/ssh/sshd_config
/bin/sed -i '113s/.*/ClientAliveInterval 3600/' /etc/ssh/sshd_config
/bin/sed -i '114s/.*/ClientAliveCountMax 0/' /etc/ssh/sshd_config
/bin/echo "Ciphers aes128-ctr,aes192-ctr,aes256-ctr" >> /etc/ssh/sshd_config
/bin/echo "MACs hmac-sha1,umac-64@openssh.com,hmac-ripemd160" >> /etc/ssh/sshd_config
/bin/echo "Ciphers aes128-ctr,aes192-ctr,aes256-ctr" >> /etc/ssh/ssh_config
/bin/echo "MACs hmac-sha1,umac-64@openssh.com,hmac-ripemd160" >> /etc/ssh/ssh_config
/bin/sed -i 's/GSSAPIAuthentication yes/GSSAPIAuthentication no/' /etc/ssh/sshd_config
/bin/systemctl restart sshd >> $LOGFILE 2>&1
/bin/sed -i '12s/.*/max_log_file = 256/' /etc/audit/auditd.conf
/bin/sed -i '13s/.*/num_logs = 7/' /etc/audit/auditd.conf
/usr/bin/systemctl enable auditd >> $LOGFILE 2>&1
/bin/sed -i 's/HISTSIZE=1000/HISTSIZE=5000/g' /etc/profile
/bin/echo "shopt -s cmdhist" >> /etc/bashrc
/bin/echo 'export HISTTIMEFORMAT="%d-%m-%Y %T "' >> /etc/bashrc
/bin/echo 'export HISTFILESIZE=5000' >> /etc/bashrc
/bin/echo 'export PROMPT_COMMAND="${PROMPT_COMMAND:+$PROMPT_COMMAND$"\n"}history -a; history -c; history -r"' >> /etc/bashrc

# zscaler part
echo -e "\n\n=======================================" >> $LOGFILE
echo "Time: `date '+%d-%m-%Y %H:%M:%S'` Installing Zscaler Certificate ..." >> $LOGFILE
echo "=======================================" >> $LOGFILE
/bin/wget -q -O /etc/pki/ca-trust/source/anchors/ZscalerRootCertificate-2048-SHA256.crt http://10.48.64.61/ZscalerRootCertificate-2048-SHA256.crt >> $LOGFILE 2>&1
/bin/update-ca-trust enable >> $LOGFILE 2>&1
/bin/update-ca-trust extract >> $LOGFILE 2>&1

# IPV6 disabling
echo -e "\n\n=======================================" >> $LOGFILE
echo "Time: `date '+%d-%m-%Y %H:%M:%S'` Disabling IPv6 ..." >> $LOGFILE
echo "=======================================" >> $LOGFILE
/bin/echo "net.ipv6.conf.all.disable_ipv6 = 1" >> /etc/sysctl.conf
/bin/echo "net.ipv6.conf.default.disable_ipv6 = 1" >> /etc/sysctl.conf
/sbin/sysctl -p >> $LOGFILE 2>&1

# flexera & bigfix agent installation
echo -e "\n\n============================================" >> $LOGFILE
echo "Time: `date '+%d-%m-%Y %H:%M:%S'` Installing Flexera/BigFix/McAfee agents ..." >> $LOGFILE
echo "============================================" >> $LOGFILE
/bin/wget -q -O /home/tcsuser/flexera.sh http://10.48.64.61/flexera.sh
/bin/sh /home/tcsuser/flexera.sh >> $LOGFILE 2>&1
/bin/wget -q -O /home/tcsuser/bigfix.sh http://10.48.64.61/bigfix.sh
/bin/sh /home/tcsuser/bigfix.sh >> $LOGFILE 2>&1
/bin/wget -q -O /home/tcsuser/mcafee.sh http://10.48.64.61/mcafee.sh
/bin/sh /home/tcsuser/mcafee.sh >> $LOGFILE 2>&1
/bin/wget -q -O /home/tcsuser/qualys.sh http://10.48.64.61/qualys.sh
/bin/sh /home/tcsuser/qualys.sh >> $LOGFILE 2>&1
#groupadd admin
#/usr/sbin/useradd -u 1201 -g admin -c "Subhayan Das" -p '$6$zOCoNEjZJw3euQ7y$U8/HnZmtsMZSMmSCnXUtOGfcEAYiFZtZ5FFOsQMRBlkJPkjIMXb.DxtSg3hwOJ3M0UBctGaK8ukD1iJfot4sq1' #tatasds
#/usr/sbin/useradd -u 1202 -g admin -c "Tamal Sett" -p '$6$zOCoNEjZJw3euQ7y$U8/HnZmtsMZSMmSCnXUtOGfcEAYiFZtZ5FFOsQMRBlkJPkjIMXb.DxtSg3hwOJ3M0UBctGaK8ukD1iJfot4sq1' #tatatas
#/usr/sbin/useradd -u 1203 -g admin -c "Anirban Adak" -p '$6$zOCoNEjZJw3euQ7y$U8/HnZmtsMZSMmSCnXUtOGfcEAYiFZtZ5FFOsQMRBlkJPkjIMXb.DxtSg3hwOJ3M0UBctGaK8ukD1iJfot4sq1' #tataana
#/bin/echo "%admin ALL=(ALL) ALL" >> /etc/sudoers
export http_proxy=http://tcsproxy:8080
export https_proxy=https://tcsproxy:8080
/bin/rpm -Uvh http://yum.spacewalkproject.org/2.7-client/RHEL/7/x86_64/spacewalk-client-repo-2.7-2.el7.noarch.rpm >> $LOGFILE 2>&1
/bin/rpm -Uvh http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm >> $LOGFILE 2>&1
/bin/yum -y install rhn-client-tools rhn-check rhn-setup rhnsd m2crypto yum-rhn-plugin rhncfg-actions osad >> $LOGFILE 2>&1
/bin/rpm -e epel-release >> $LOGFILE 2>&1
/bin/rpm -e spacewalk-client-repo-2.7-2.el7.noarch >> $LOGFILE 2>&1
unset http_proxy
unset https_proxy
/bin/wget -q -O /usr/share/rhn/RHN-ORG-TRUSTED-SSL-CERT http://10.59.19.170/pub/RHN-ORG-TRUSTED-SSL-CERT
/sbin/rhnreg_ks --activationkey 1-7955029135f506b26499e03c44be2211 --serverUrl https://dkdc2patprdl002/XMLRPC
/usr/bin/systemctl enable osad >> $LOGFILE 2>&1
/usr/bin/systemctl restart osad
/sbin/osad
/bin/rhn-actions-control --enable-all

# Domain joinin part dns entry 
echo -e "\n\n=======================================" >> $LOGFILE
echo "Time: `date '+%d-%m-%Y %H:%M:%S'` Domain Joining and DNS Entry ..." >> $LOGFILE
echo "=======================================" >> $LOGFILE
/bin/wget -q -O /home/tcsuser/domainjoin.sh http://10.48.64.61/domainjoin.sh
/bin/sh /home/tcsuser/domainjoin.sh >> $LOGFILE 2>&1
groupadd admin
realm deny --all >> $LOGFILE 2>&1
realm permit tatatas >> $LOGFILE 2>&1
realm permit tatasds >> $LOGFILE 2>&1
realm permit tataana >> $LOGFILE 2>&1
realm permit mcafee.admin >> $LOGFILE 2>&1
realm permit sa01.ignio >> $LOGFILE 2>&1
realm permit sa01.ECPSolarwinds >> $LOGFILE 2>&1
/bin/echo "%admin ALL=(ALL) ALL" >> /etc/sudoers
/bin/echo "sa01.ECPSolarwinds ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
/bin/echo "sa01.ignio ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
usermod -a -G admin tatatas >> $LOGFILE 2>&1
usermod -a -G admin tatasds >> $LOGFILE 2>&1
usermod -a -G admin tataana >> $LOGFILE 2>&1
usermod -a -G admin mcafee.admin >> $LOGFILE 2>&1
yum update -y >> $LOGFILE 2>&1
systemctl restart sssd >> $LOGFILE 2>&1
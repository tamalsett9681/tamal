#!/bin/sh

HOST=`hostname`

# Disable Firewall
systemctl stop firewalld
systemctl disable firewalld
sed -i '7s/.*/SELINUX=disabled/' /etc/sysconfig/selinux
sed -i '7s/.*/SELINUX=disabled/' /etc/selinux/config
setenforce 0

#creating local users, which exists
groupadd admin
useradd -u 1201 -g admin -c "TCS System Admin" -p '$6$r2UWxo/bw4iCMjY5$s5vxwh3P15mvdCyjlZjxpP83OP2RgU5NWg.EVIBAEzIDGhJ.fbBz91lTMzNz0PpvzGwgzuCHbl4pwG29xZBA8.' tcsuser

#setting admin access
/bin/echo "%admin ALL=(ALL) ALL" >> /etc/sudoers

#setting timezone
/usr/bin/rm -rf /etc/localtime
/usr/bin/ln -s /usr/share/zoneinfo/Europe/Copenhagen /etc/localtime

# changin the ethernet configuratoin
/bin/sed -i '6s/.*/PEERDNS=no/' /etc/sysconfig/network-scripts/ifcfg-eth0
/bin/echo "DNS1=10.90.1.11" >> /etc/sysconfig/network-scripts/ifcfg-eth0
/bin/echo "DNS2=10.80.1.11" >> /etc/sysconfig/network-scripts/ifcfg-eth0
/bin/echo "DNS3=10.48.22.11" >> /etc/sysconfig/network-scripts/ifcfg-eth0
/bin/echo "DOMAIN=prd1.prdroot.net" >> /etc/sysconfig/network-scripts/ifcfg-eth0
/usr/bin/systemctl restart network

#Installing yum packages
yum install sssd realmd oddjob oddjob-mkhomedir adcli samba-common samba-common-tools krb5-workstation chrony unzip nmap telnet -y

# NTP Configuration
/bin/sed -i '2s/.*/#&/' /etc/chrony.conf
/bin/sed -i '3s/.*/#&/' /etc/chrony.conf
/bin/sed -i '4s/.*/#&/' /etc/chrony.conf
/bin/sed -i '5s/.*/#&/' /etc/chrony.conf
/bin/echo "server 10.90.1.16" >> /etc/chrony.conf 
/usr/bin/systemctl enable chronyd
/usr/bin/systemctl restart chronyd
/sbin/ntpdate -u 10.90.1.16 
/bin/chronyc makestep

# changing password aging
/bin/sed -i '25s/.*/PASS_MAX_DAYS   90/' /etc/login.defs
/bin/sed -i '26s/.*/PASS_MIN_DAYS   7/' /etc/login.defs
/bin/sed -i '27s/.*/PASS_MIN_LEN    8/' /etc/login.defs
/bin/sed -i '28s/.*/PASS_WARN_AGE   28/' /etc/login.defs
/bin/sed -i '29s/.*/LOGIN_RETRIES   5\n/' /etc/login.defs


# setting password complexity
/bin/sed -i '6s/.*/auth            required        pam_wheel.so root_only use_uid debug/' /etc/pam.d/su
/usr/bin/systemctl enable --now oddjobd
/sbin/authconfig --passalgo=sha512 --passminlen=9 --passminclass=3 --passmaxrepeat=3 --passmaxclassrepeat=4 --enablerequpper --enablereqlower --enablereqdigit --enablereqother --enablefaillock  --faillockargs="audit deny=5 fail_interval=900 unlock_time=0" --enablemkhomedir --update
/bin/echo "difok = 5" >> /etc/security/pwquality.conf
/bin/echo "gecoscheck = 1" >> /etc/security/pwquality.conf
/bin/wget -q -O /etc/pam.d/system-auth-tryg http://10.90.1.16/system-auth-tryg
/bin/wget -q -O /etc/pam.d/password-auth-tryg http://10.90.1.16/password-auth-tryg
cd /etc/pam.d; unlink system-auth; unlink password-auth; ln -s system-auth-tryg system-auth; ln -s password-auth-tryg password-auth; cd ~
/bin/sed -i '81i\Defaults    logfile = /var/log/sudo.log' /etc/sudoers

# changing ssh configuration
/bin/sed -i '39i\PermitRootLogin no' /etc/ssh/sshd_config
/bin/sed -i '113s/.*/ClientAliveInterval 3600/' /etc/ssh/sshd_config
/bin/sed -i '114s/.*/ClientAliveCountMax 0/' /etc/ssh/sshd_config
/bin/echo "Ciphers aes128-ctr,aes192-ctr,aes256-ctr" >> /etc/ssh/sshd_config
/bin/echo "MACs hmac-sha1,umac-64@openssh.com,hmac-ripemd160" >> /etc/ssh/sshd_config
/bin/echo "Ciphers aes128-ctr,aes192-ctr,aes256-ctr" >> /etc/ssh/ssh_config
/bin/echo "MACs hmac-sha1,umac-64@openssh.com,hmac-ripemd160" >> /etc/ssh/ssh_config
/bin/sed -i 's/GSSAPIAuthentication yes/GSSAPIAuthentication no/' /etc/ssh/sshd_config
/bin/systemctl restart sshd
/bin/sed -i '12s/.*/max_log_file = 256/' /etc/audit/auditd.conf
/bin/sed -i '13s/.*/num_logs = 7/' /etc/audit/auditd.conf
/usr/bin/systemctl enable auditd
/bin/sed -i 's/HISTSIZE=1000/HISTSIZE=5000/g' /etc/profile
/bin/echo "shopt -s cmdhist" >> /etc/bashrc
/bin/echo 'export HISTTIMEFORMAT="%d-%m-%Y %T "' >> /etc/bashrc
/bin/echo 'export HISTFILESIZE=5000' >> /etc/bashrc
/bin/echo 'export PROMPT_COMMAND="${PROMPT_COMMAND:+$PROMPT_COMMAND$"\n"}history -a; history -c; history -r"' >> /etc/bashrc

# zscaler part
/bin/wget -q -O /etc/pki/ca-trust/source/anchors/ZscalerRootCertificate-2048-SHA256.crt http://10.90.1.16/ZscalerRootCertificate-2048-SHA256.crt 
/bin/update-ca-trust enable 
/bin/update-ca-trust extract

# bppm part
#/bin/wget -q -O /etc/systemd/system/bppm.service http://10.90.1.16/bppm.service
/bin/chmod +x /etc/systemd/system/bppm.service
mkdir /opt/bmc
chown bppm:bppm /opt/bmc

# IPV6 disabling
/bin/echo "net.ipv6.conf.all.disable_ipv6 = 1" >> /etc/sysctl.conf
/bin/echo "net.ipv6.conf.default.disable_ipv6 = 1" >> /etc/sysctl.conf
/sbin/sysctl -p

# flexera & bigfix agent installation
/bin/wget -q -O /home/tcsuser/flexera.sh http://10.90.1.16/flexera.sh
/bin/sh /home/tcsuser/flexera.sh
/bin/wget -q -O /home/tcsuser/bigfix.sh http://10.90.1.16/bigfix.sh
/bin/sh /home/tcsuser/bigfix.sh 
/bin/wget -q -O /home/tcsuser/mcafee.sh http://10.90.1.16/mcafee.sh
/bin/sh /home/tcsuser/mcafee.sh

# Data file system creation
/bin/wget -q -O /home/tcsuser/disk.sh http://10.90.1.16/disk.sh
/bin/sh /home/tcsuser/disk.sh

# Domain joinin part dns entry 
#ADDR=`hostname -i`
#HOST=`hostname`
#echo "update add $HOST.PRD1.PRDROOT.NET 3600 A $ADDR" >> /var/nsupdate.txt
#echo "send" >> /var/nsupdate.txt
#echo "quit" >> /var/nsupdate.txt
#nsupdate /var/nsupdate.txt

## echo $2 | adcli preset-computer --domain=prd1.prdroot.net -U $1  -O 'OU=Azure,OU=Servers,OU=TCS,DC=prd1,DC=prdroot,DC=net' $HOST --stdin-password
## sleep 300
echo $2 | realm join --computer-ou='OU=Azure,OU=Servers,OU=TCS,DC=prd1,DC=prdroot,DC=net' --user $1 prd1.prdroot.net --install=/
/bin/sed -i '16s/.*/use_fully_qualified_names = False/' /etc/sssd/sssd.conf
/bin/sed -i '17s/.*/fallback_homedir = \/home\/%u/' /etc/sssd/sssd.conf
/bin/sed -i 's/cache_credentials = True/cache_credentials = False/g' /etc/sssd/sssd.conf
/bin/sed -i '14i\krb5_validate = False' /etc/sssd/sssd.conf
/bin/echo "dyndns_update = True" >> /etc/sssd/sssd.conf
/bin/echo "dyndns_refresh_interval = 43200" >> /etc/sssd/sssd.conf
/bin/echo "dyndns_ttl = 3600" >> /etc/sssd/sssd.conf
/bin/echo "dyndns_iface = eth0" >> /etc/sssd/sssd.conf
/bin/echo "ad_hostname = `hostname`.prd1.prdroot.net" >> /etc/sssd/sssd.conf
/bin/systemctl restart sssd


## Command to Disable All AD User login ##
realm deny --all
realm permit tatatas
realm permit tatasds
realm permit tataana
usermod -a -G admin tatatas
usermod -a -G admin tatasds
usermod -a -G admin tataana

## Configuring Swap and update OS ##
/bin/yum update -y
/bin/wget -q -O /home/tcsuser/waagent.sh http://10.90.1.16/waagent.sh
/bin/sh /home/tcsuser/waagent.sh